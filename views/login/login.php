<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@300&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo constant('URL'); ?>resource/css/login.css">
    
    <title>Login</title>
    
</head>
<body>
    
    <?php $this->showMessages();?>
    <div class="d-flex justify-content-center">
		<div class="icon">
            <span class="brand_logo"><i class="fas fa-user"></i></span>
		</div>
	</div>
    <form action="<?php echo constant('URL'); ?>login/authenticate" method="POST">
        <div><?php (isset($this->errorMessage))?  $this->errorMessage : '' ?></div>
        <div style="margin-top: 100px;" class="input-group form-group">
		    <div class="input-group-prepend">
				<span style="height: 50px;" class="input-group-text"><i class="fas fa-user"></i></span>
			</div>
			    <input style="height: 50px; font-family: 'Poppins', sans-serif;" type="text"
                 name="username" id="username" class="form-control" placeholder="Usuario">	
		</div>
        <div style="margin-top: 20px;" class="input-group form-group">
			<div class="input-group-prepend">
				<span style="height: 50px;" class="input-group-text"><i class="fas fa-key"></i></span>
			</div>
		    <input style="height: 50px; font-family: 'Poppins', sans-serif;" 
            type="password" name="password" id="password" class="form-control" placeholder="Contraseña">
		</div>
        <button style="margin-bottom: 5px; margin-top: 25px; background: #4B6587; font-size:20px" 
        type="submit" class="btn btn-primary">Iniciar Sesión</button>
        
        <p class="h6" style="padding-top: 20px;">
            ¿No tienes cuenta? <a href="<?php echo constant('URL'); ?>register"><strong>Registrarse<strong</a>
        </p>
    </form>

    <script src="<?php echo constant('URL'); ?>resource/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
</body>
</html>