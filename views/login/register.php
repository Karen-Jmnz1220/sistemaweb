<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@300&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo constant('URL'); ?>resource/css/register.css">
    
    <title>Registro</title>
</head>
<body>
    <?php $this->showMessages();?>
    <div class="d-flex justify-content-center">
		<div class="icon">
            <span class="brand_logo"><i class="fas fa-user"></i></span>
		</div>
	</div>
    <form action="<?php echo constant('URL'); ?>register/newUser" method="POST">
        
        <div style="margin-top: 100px; margin-bottom: 10px;" class="form-floating">
            <input style="height: 45px;" type="text" class="form-control" name="name" id="name" placeholder="Name">
            
        </div>
        <div style="margin-bottom: 10px;" class="form-floating">
            <input style="height: 45px;" type="text" class="form-control" name="surname" id="surname" placeholder="Surname">
            
        </div>
        <div style="margin-bottom: 10px;" class="form-floating">
            <input style="height: 45px;" type="text" class="form-control" name="username" id="username" placeholder="username98">
            
        </div>
        <div style="margin-bottom: 10px;" class="form-floating">
            <input style="height: 45px;" type="password" class="form-control" name="password" id="password" placeholder="Password">
            
        </div>
        <button style="margin-bottom: 5px; margin-top: 15px; background: #4B6587; font-size:20px" type="submit" class="btn btn-primary">Registrarse</button>
        <p class="h6" style="padding-top: 10px;">
        ¿Ya tienes cuenta? <a href="<?php echo constant('URL'); ?>login"><strong>Iniciar Sesión<strong</a>
        </p>
            
    </form>
    
    <script src="<?php echo constant('URL'); ?>resource/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
</body>
</html>
