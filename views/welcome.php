<?php
    $departments = $this->d['departments'];
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />

        <title>Bienvenida</title>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
        <!-- Google fonts-->
        <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@300&display=swap" rel="stylesheet">
        <!-- Core theme CSS (includes Bootstrap)-->
        <link href="<?php echo constant('URL'); ?>resource/css/welcome.css" rel="stylesheet" />
    </head>
    
    <body id="page-top">
        <!-- Navigation-->
        <nav class="navbar navbar-expand-lg bg-secondary text-uppercase fixed-top" id="mainNav">
            <div class="container">
                <a class="navbar-brand">Trámites-Salvatierra</a>

                <div class="d-grid gap-2 d-md-flex justify-content-md-end">
                    <a class="btn btn-outline-success"  href="<?php echo constant('URL'); ?>login">Iniciar Sesión</a>
                    <a class="btn btn-outline-success"  href="<?php echo constant('URL'); ?>register">Registrarse</a>
                </div>
                
            </div>
        </nav>
        <!-- Masthead-->
        <header class="masthead bg-primary text-white text-center">
            <div  class="container d-flex align-items-center flex-column">
                <!-- Masthead Heading-->
                <h1  class="masthead-heading text-uppercase mb-0">Bienvenido a Trámites-Salvatierra</h1>
                
                <!-- Icon Divider-->
                <div class="divider-custom divider-light">
                    <div class="divider-custom-line"></div>
                    <div class="divider-custom-icon"><i   class="fas fa-star"></i></div>
                    <div class="divider-custom-line"></div>
                </div>
                <!-- Masthead Subheading-->
                <strong><p  class="masthead-subheading font-weight-light mb-0">Elige el Departamento o Trámite que deseas realizar e inicia
                    sesión.
                </p></strong>
            </div>
            
            <form id="formDeptsForms" action="<?php echo constant('URL'); ?>welcome/authenticate" method="POST">
                <div style="text-align: center;">
                    <label style="padding-bottom: 10px; padding-top:30px; color:white; font-size: 20px">Departamento
                    <span class="text-danger">*</span></label>
                    <center>
                        <select id="departments" name="departments" class="js-select2 form-control" aria-label="Default select example" 
                        style="width: 500px;">
                            <option vzlue="0" selected>Seleccione un departamento</option>
                            <?php 
                                foreach ($departments as $dept) {
                            ?>
                            <option value="<?php echo $dept->getIdDept() ?>"><?php echo $dept->getName_dept() ?></option>
                            <?php
                                }
                            ?>
                        </select>
                    </center>
                </div>
                <div style="text-align: center;">
                    <label style='padding-bottom: 10px; padding-top:30px; color:white; font-size: 20px'>Trámite<span class='text-danger'>*</span></label>
                    <center>
                    <select id='formalities' name='formalities' class='js-select2 form-control' aria-label='Default select example' style='width: 500px;'>
                        <option value="0" selected>Selecciona un trámite</option>
                    </select> 
                    </center>                    
                </div>
                <button style="margin-top: 30px; width:180px" type="submit" class="btn btn-outline-secondary" id="btnDetails" data-bs-toggle="modal" 
                data-bs-target="#detailsModal">Ver detalles</button>
            </form>
            
        </header>

        <div class="modal" tabindex="-1" id="detailsModal">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Detalles del tramite</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        <p>¿Desea visualizar los detalles del tramite y posterior a ello iniciar sesión?</p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cancelar</button>
                        <a class="btn btn-primary" href="<?php echo constant('URL'); ?>logout">Ver detalles</a>
                    </div>
                </div>
            </div>
        </div>
        
        <script type="text/javascript">
            $(document).ready(function(){
            
                $('#departments').change(function(){
                    $("#departments option:selected").each(function () {
                        idDept = $(this).val();
                        console.log(idDept);
                        $.ajax({
                        url : '<?php echo URL; ?>/welcome/getFormalitie/',
                        type: "POST",
                        data : {'idDept':idDept}
                        })
                        .done(function(data) {
                            $("#formalities").html(data);
                        });
                        
                    }); 
                });
                $("#formalities").change(function(){
                    $("#formalities option:selected").each(function () {
                        idFormalitie= $(this).val();
                        console.log(idFormalitie);                       
                             
                    });
                });
            
                
            });
        </script>
    </body>
</html>