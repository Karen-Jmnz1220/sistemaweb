<?php
     $user = $this->d['user'];
     $manager = $this->d['manager'];
    require_once 'parte_superior.php';
?>
<!-- inicio del contenido principal -->
<div class="container">

        <div class="row">
                <div class="col-lg-12">
                    <div class="table-responsive">        
                    <table id="tablaEditar" class="table table-striped table-bordered table-condensed" style="width:100%">
                        <thead class="text-center">
                            <tr style="color:blue;">
                                <th>ID</th>
                                <th>NOMBRE_DEPT</th>
                                <th>ID_MANAGER</th>                                
                                <th>DIRECCION</th>  
                                <th>TELEFONO</th>
                                <th>CORREO ELECTRONICO</th>
                                <th>ACCION</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            include_once 'models/deptmodel.php';             
                                foreach($this->departments as $array){
                                    $department = new DepartmentsModel();                                                   
                                    $department = $array;
                                
                            ?>
                            <tr>
                                <td><?php echo $department->getIdDept(); ?></td>
                                <td><?php echo $department->getName_dept(); ?></td>
                                <td><?php echo $department->getIdManager(); ?></td>
                                <td><?php echo $department->getAddress(); ?></td>
                                <td><?php echo $department->getPhone(); ?></td>
                                <td><?php echo $department->getEmail(); ?></td>
                                <td></td>
                            </tr>
                            <?php
                                }
                            ?>                                
                        </tbody>        
                       </table>                     
                    </div>
                </div>
        </div>

</div>  

<div class="modal fade" id="modalCRUD" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel"></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
            </div>
        
            <form id="formDepts" method="POST">
                <h2 style="text-align: center;">Editar Departamento</h2>    
                <div class="modal-body">

                    <div style="margin-top: 80px; margin-bottom: 10px;" class="form-floating">
                        <input type="text" class="form-control" name="name_dept" id="name_dept" placeholder="Nombre" required>
                    </div>

                    <div style="margin-bottom: 10px;" class="form-floating">
                        <select id="lista1" class="custom-select" aria-label="Default select example" style="width: 465px;" name="idManager">
                                <option selected>Seleccione un encargado</option>
                                <?php 
                                    foreach ($manager as $man) {
                                ?>
                                    <option value="<?php echo $man->getIdUser() ?>"><?php echo $man->getName() ?> <?php echo $man->getSurname() ?></option>
                                        <?php
                                    }
                                ?>
                        </select>
                    </div>

                    <div style="margin-bottom: 10px;" class="form-floating">
                        <input type="text" class="form-control" name="address" id="address" placeholder="Dirección" required>   
                    </div>

                    <div style="margin-bottom: 10px;" class="form-floating">
                        <input type="text" class="form-control" name="phone" id="phone" placeholder="Telefono" required>
                    </div>

                    <div style="margin-bottom: 10px;" class="form-floating">
                        <input type="email" class="form-control" name="email" id="email" placeholder="Correo electronico" required>
                    </div>

                    <div class="modal-footer">
                            <button type="button" class="btn btn-light" data-dismiss="modal">Cancelar</button>
                            <button style=" background: #4B6587;" type="submit" id="btnGuardar" class="btn btn-dark" >Guardar</button>
                    </div>
                </div>
            </form> 
        </div>
    </div>
</div> 

<?php
    require_once 'parte_inferior.php'
?>

