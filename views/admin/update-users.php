<?php
     $user = $this->d['user'];
    require_once 'parte_superior.php'
?>
<!-- inicio del contenido principal -->
<div class="container">

        <div class="row">
                <div class="col-lg-12">
                    <div class="table-responsive">        
                    <table id="tablaEditar" class="table table-striped table-bordered table-condensed" style="width:100%">
                        <thead class="text-center">
                            <tr style="color:blue;">
                                <th >ID</th>
                                <th>NOMBRE</th>
                                <th>APELLIDO</th>                                
                                <th>USUARIO</th>  
                                <th>CONTRASEÑA</th>
                                <th>ROL</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            include_once 'models/usermodel.php';             
                                foreach($this->users as $array){
                                    $users = new UserModel();                                                   
                                    $user = $array;
                                
                            ?>
                            <tr>
                                <td><?php echo $user->getIdUser(); ?></td>
                                <td><?php echo $user->getName(); ?></td>
                                <td><?php echo $user->getSurname(); ?></td>
                                <td><?php echo $user->getUsername(); ?></td>
                                <td><?php echo $user->getPassword(); ?></td>
                                <td><?php echo $user->getRol_idRol(); ?></td>
                            </tr>
                            <?php
                                }
                            ?>                                
                        </tbody>           
                       </table>                    
                    </div>
                </div>
        </div>

</div>  

<div class="modal fade" id="modalCRUD" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel"></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
            </div>
        <form id="formUsers"  method="POST">
            <h2 style="text-align: center;">Editar Usuario</h2>    
            <div class="modal-body">
                <div class="form-group">
                    <label for="name" class="col-form-label">Nombre:</label>
                    <input type="text" class="form-control" name="name" id="name" required>
                </div>

                <div class="form-group">
                    <label for="surname" class="col-form-label">Apellido:</label>
                    <input type="text" class="form-control" name="surname" id="surname" required>
                </div>  

                <div class="form-group">
                    <label for="username" class="col-form-label">Usuario:</label>
                    <input type="text" class="form-control" name="username" id="username" required>
                </div>
                
                <div class="form-group">
                    <label for="password" class="col-form-label">Contraseña:</label>
                    <input type="password" class="form-control" name="password" id="password" required>
                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-light" data-dismiss="modal">Cancelar</button>
                <button style=" background: #4B6587;" type="submit" id="btnGuardarUsers" class="btn btn-dark" >Guardar</button>
            </div>
        </form>    
        </div>
    </div>
</div>  
      

<?php
    require_once 'parte_inferior.php'
?>
