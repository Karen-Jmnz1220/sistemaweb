<?php
     $user = $this->d['user'];
    require_once 'parte_superior.php'
?>
<!-- inicio del contenido principal -->
<div class="container">
    <div class="row">
        <div class="col-lg-12">
            <div class="table-responsive">        
                <table id="tabla" class="table table-striped table-bordered table-condensed" style="width:100%">
                    <thead class="text-center">
                        <tr style="color:blue;">
                            <th >ID</th>
                            <th>NOMBRE</th>
                            <th>APELLIDO</th>                                
                            <th>USUARIO</th>  
                            <th>CONTRASEÑA</th>
                            <th>ROL</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                            include_once 'models/usermodel.php';             
                                foreach($this->users as $array){
                                    $users = new UserModel();                                                   
                                    $user = $array;                    
                        ?>
                        <tr>
                            <td><?php echo $user->getIdUser(); ?></td>
                            <td><?php echo $user->getName(); ?></td>
                            <td><?php echo $user->getSurname(); ?></td>
                            <td><?php echo $user->getUsername(); ?></td>
                            <td><?php echo $user->getPassword(); ?></td>
                            <td><?php echo $user->getRol_idRol(); ?></td>
                        </tr>
                        <?php
                        }
                        ?>                                
                    </tbody>           
                </table>                    
            </div>
        </div>
    </div>  
</div>    

<!-- FIN -->
<?php
    require_once 'parte_inferior.php'
?>
 