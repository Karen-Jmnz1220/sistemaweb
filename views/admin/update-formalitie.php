<?php
     $user = $this->d['user'];
    require_once 'parte_superior.php'
?>
<!-- inicio del contenido principal -->
<div class="container">

    <div class="row">
        <div class="col-lg-12">
            <div class="table-responsive">        
                <table id="tablaEditar" class="table table-striped table-bordered table-condensed" style="width:100%">
                    <thead class="text-center">
                        <tr style="color:blue;">
                            <th>ID</th>
                            <th>NOMBRE</th>
                            <th>DESCRIPCIÓN</th>                                
                            <th>ID_DEPARTAMENTO</th>  
                            <th>COSTO</th>
                            <th>ACCIÓN</th>
                        </tr>
                    </thead>
                    <tbody id="databody">
                        <?php
                            include_once 'models/formalitiesmodel.php';             
                            foreach($this->formalities as $array){
                                $formalitie = new FormalitiesModel();                                                   
                                $formalitie = $array;           
                        ?>
                        <tr>
                            <td><?php echo $formalitie->getIdFormalitie(); ?></td>
                            <td><?php echo $formalitie->getFormalitie_name(); ?></td>
                            <td><?php echo $formalitie->getDescription(); ?></td>
                            <td><?php echo $formalitie->getIdDept(); ?></td>
                            <td><?php echo $formalitie->getCost(); ?></td>
                            <td></td>
                        </tr>
                        <?php
                            }
                        ?>                                     
                    </tbody>           
                </table>                    
            </div>
        </div>
    </div>

</div>

<div class="modal fade" id="modalCRUD" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel"></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
            </div>
        
            <form id="formFormalities" method="POST">
                <h2 style="text-align: center;">Editar Trámite</h2>    
                <div class="modal-body">

                    <div style="margin-top: 80px; margin-bottom: 10px;" class="form-floating">
                        <input type="text" class="form-control" name="formalitie_name" id="formalitie_name" placeholder="Nombre" required>
                    </div>

                    <div style="margin-bottom: 10px;" class="form-floating">
                        <input type="text" class="form-control" name="description" id="description" placeholder="Descripción" required>
                    </div>

                    <div style="margin-bottom: 10px;" class="form-floating">
                        <input type="text" class="form-control" name="idDept" id="idDept" placeholder="ID del departamento" required>   
                    </div>

                    <div style="margin-bottom: 10px;" class="form-floating">
                        <input type="text" class="form-control" name="cost" id="cost" placeholder="Costo" required>
                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-light" data-dismiss="modal">Cancelar</button>
                        <button style=" background: #4B6587;" type="submit" id="btnGuardarForms" class="btn btn-dark" >Guardar</button>
                    </div>
                </div>
            </form> 
        </div>
    </div>
</div> 


<?php
    require_once 'parte_inferior.php'
?>
