<?php
     $user = $this->d['user'];
    require_once 'parte_superior.php'
?>
<!-- inicio del contenido principal -->
<div class="container">

        <div class="row">
                <div class="col-lg-12">
                    <div class="table-responsive">        
                        <table id="tabla" class="table table-striped table-bordered table-condensed" style="width:100%">
                        <thead class="text-center">
                            <tr style="color:blue;">
                                <th>ID</th>
                                <th>NOMBRE</th>
                                <th>DESCRIPCIÓN</th>                                
                                <th>ID_DEPARTAMENTO</th>  
                                <th>COSTO</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            include_once 'models/formalitiesmodel.php';             
                                foreach($this->formalities as $array){
                                    $formalitie = new FormalitiesModel();                                                   
                                    $formalitie = $array;
                                
                            ?>
                            <tr>
                                <td><?php echo $formalitie->getIdFormalitie(); ?></td>
                                <td><?php echo $formalitie->getFormalitie_name(); ?></td>
                                <td><?php echo $formalitie->getDescription(); ?></td>
                                <td><?php echo $formalitie->getIdDept(); ?></td>
                                <td><?php echo $formalitie->getCost(); ?></td>
                            </tr>
                            <?php
                                }
                            ?>                                
                        </tbody>        
                       </table>                    
                    </div>
                </div>
        </div>  
</div>    

<!-- FIN -->
<?php
    require_once 'parte_inferior.php'
?>