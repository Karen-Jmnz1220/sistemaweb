<?php
     $user = $this->d['user'];
    require_once 'parte_superior.php'
?>
<!-- inicio del contenido principal -->
<div class="container">

        <div class="row">
                <div class="col-lg-12">
                    <div class="table-responsive">        
                        <table id="tablaPersonas" class="table table-striped table-bordered table-condensed" style="width:100%">
                        <thead class="text-center">
                            <tr style="color:blue;">
                                <th data-sort="idDept">ID</th>
                                <th data-sort="name_dept">NOMBRE_DEPT</th>
                                <th data-sort="idManager">ID_MANAGER</th>                                
                                <th data-sort="address">DIRECCION</th>  
                                <th data-sort="phone">TELEFONO</th>
                                <th data-sort="email">CORREO ELECTRONICO</th>
                                <th>ACCIONES</th>
                            </tr>
                        </thead>
                        <tbody id="databody">
                                                       
                        </tbody>           
                       </table>                    
                    </div>
                </div>
        </div>

</div>  

<script>
        var data = [];
        var copydata = [];
        const sorts = document.querySelectorAll('th');

        sorts.forEach(item =>{
            item.addEventListener('click', e =>{
                if(item.dataset.sort){  
                        sortBy(item.dataset.sort);        
                }
            });
        });

        async function getData(){
            
            data = await fetch('http://localhost/sistemaweb//admin/getHistoryJSONDepts')
            .then(res =>res.json())
            .then(json => json);
            this.copydata = [...this.data];
            console.table(data);
            renderData(data);
            console.log(data);  
        }
        getData();

        function renderData(data){
            var databody = document.querySelector('#databody');
            let total = 0;
            databody.innerHTML = '';
            data.forEach(item => { 
                //total += item.amount;
                databody.innerHTML += `<tr>
                        <td>${item.idDept}</td>
                        <td>${item.name_dept}</td>
                        <td>${item.idManager}</td>
                        <td>${item.address}</td>
                        <td>${item.phone}</td>
                        <td>${item.email}</td>
                        <td><a href="http://localhost/sistemaweb/admin/deleteDept/${item.idDept}">Eliminar</a></td>
                    </tr>`;
            });
        }
        
</script>

<?php
    require_once 'parte_inferior.php'
?>
