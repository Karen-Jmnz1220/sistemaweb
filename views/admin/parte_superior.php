<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Admin - Dashboard</title>

    <!-- Custom fonts for this template-->
    <link href="<?php echo constant('URL'); ?>resource/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@300&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
    
    <!-- Custom styles for this template-->
    <link href="<?php echo constant('URL'); ?>resource/css/admin.css" rel="stylesheet">

    <!--datables CSS básico-->
    <link rel="stylesheet" type="text/css" href="<?php echo constant('URL'); ?>resource/vendor/datatables/datatables.min.css"/>
    <!--datables estilo bootstrap 4 CSS-->  
    <link rel="stylesheet"  type="text/css" href="<?php echo constant('URL'); ?>resource/vendor/datatables/DataTables-1.10.18/css/dataTables.bootstrap4.min.css">  

</head>

<body id="page-top">

    <!-- Page Wrapper -->
    <div id="wrapper">

        <!-- Sidebar -->
        <ul style="width: 50px;" class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

            <!-- Sidebar - Brand -->
            <a class="sidebar-brand d-flex align-items-center justify-content-center" href="index.html">
                <div class="sidebar-brand-icon rotate-n-15">
                    <i class="fas fa-laugh-wink"></i>
                </div>
                <div class="sidebar-brand-text mx-3">Administrador</div>
            </a>

            <!-- Divider -->
            <hr class="sidebar-divider my-0">

            <!-- Heading -->
            <div style="margin-top: 50px;" class="sidebar-heading">
                Interface
            </div>

            <!-- Nav Item - Pages Users Menu -->
            <li class="nav-item">
                <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseTwo"
                    aria-expanded="true" aria-controls="collapseTwo">
                    <i class="fas fa-user"></i>
                    <span style="font-size: 15px;">Usuarios</span>
                </a>
                <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
                    <div class="bg-white py-2 collapse-inner rounded">
                        <h6 class="collapse-header">Acciones:</h6>
                        <a class="collapse-item" href="<?php echo constant('URL'); ?>/admin/showUsers">Visualizar</a>
                        <a class="collapse-item" href="<?php echo constant('URL'); ?>/admin/createUsers">Registrar</a>
                        <a class="collapse-item" href="<?php echo constant('URL'); ?>/admin/updateUsers">Modificar</a>
                        <a class="collapse-item" href="<?php echo constant('URL'); ?>/admin/deleteUsers">Eliminar</a>
                    </div>
                </div>
            </li>

            <!-- Nav Item - Depts Collapse Menu -->
            <li class="nav-item">
                <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseUtilities"
                    aria-expanded="true" aria-controls="collapseUtilities">
                    <i class="fas fa-desktop"></i>
                    <span style="font-size: 15px;">Departamentos</span>
                </a>
                <div id="collapseUtilities" class="collapse" aria-labelledby="headingUtilities"
                    data-parent="#accordionSidebar">
                    <div class="bg-white py-2 collapse-inner rounded">
                        <h6 class="collapse-header">Acciones:</h6>
                        <a class="collapse-item" href="<?php echo constant('URL'); ?>/admin/showDept">Visualizar</a>
                        <a class="collapse-item" href="<?php echo constant('URL'); ?>/admin/createDept">Registrar</a>
                        <a class="collapse-item" href="<?php echo constant('URL'); ?>/admin/updateDepts">Modificar</a>
                        <a class="collapse-item" href="<?php echo constant('URL'); ?>/admin/deleteDepts">Eliminar</a>
                    </div>
                </div>
            </li>

            <!-- Nav Item - Formalities Collapse Menu -->
            <li class="nav-item">
                <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapsePages"
                    aria-expanded="true" aria-controls="collapsePages">
                    <i class="fas fa-desktop"></i>
                    <span style="font-size: 15px;">Trámites</span>
                </a>
                <div id="collapsePages" class="collapse" aria-labelledby="headingPages" data-parent="#accordionSidebar">
                    <div class="bg-white py-2 collapse-inner rounded">
                    <h6 class="collapse-header">Acciones:</h6>
                        <a class="collapse-item" href="<?php echo constant('URL'); ?>/admin/showForm">Visualizar</a>
                        <a class="collapse-item" href="<?php echo constant('URL'); ?>/admin/createForm">Registrar</a>
                        <a class="collapse-item" href="<?php echo constant('URL'); ?>/admin/updateFormalities">Modificar</a>
                        <a class="collapse-item" href="<?php echo constant('URL'); ?>/admin/deleteFormalities">Eliminar</a>
                    </div>
                </div>
            </li>
           
            <!-- Sidebar Toggler (Sidebar) -->
            <div class="text-center d-none d-md-inline">
                <button class="rounded-circle border-0" id="sidebarToggle"></button>
            </div>

            <!-- Sidebar Message -->
        </ul>
        <!-- End of Sidebar -->

        <!-- Content Wrapper -->
        <div id="content-wrapper" class="d-flex flex-column">

            <!-- Main Content -->
            <div id="content">

                <!-- Topbar -->
                <nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">
                <li style="margin-left: 1100px;" class="nav-item dropdown no-arrow">
                            <a class="nav-link dropdown-toggle" href="<?php echo constant('URL');?>user" id="userDropdown" role="button"
                                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <div class="name"><h5><?php echo $user->getName(); ?></h5></div>
                                <div class="photo"><h5>
                                    <?php  if($user->getPhoto() == ''){?>
                                        <i style="size: 20px;" class="far fa-user-circle"></i>
                                    <?php }else{ ?>
                                            <img src="<?php echo constant('URL'); ?>public/img/photos/<?php echo $user->getPhoto() ?>" width="32" />
                                    <?php }  ?></h5>
                                </div>
                            </a>
                            <!-- Dropdown - User Information -->
                            <div style="margin-left: 500px;" class="dropdown-menu dropdown-menu-right shadow animated--grow-in"
                                aria-labelledby="userDropdown">
                                
                                <a class="dropdown-item" data-toggle="modal" data-target="#logoutModal">
                                    <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>
                                    Logout
                                </a>
                            </div>
                        </li>    
                </nav>
                <!-- End of Topbar -->
            