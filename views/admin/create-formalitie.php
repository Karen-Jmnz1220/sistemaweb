<?php
     $user = $this->d['user'];
    require_once 'parte_superior.php'
?>
<!-- inicio del contenido principal -->
<div class="container">
    <h1>Registro de Trámites</h1>
    <form style="margin-left: 50px; margin-right:50px" method="POST" action="<?php echo constant('URL'); ?>admin/newFormalitie">
        <div style="margin-top: 70px; margin-bottom: 10px;" class="form-floating">
            <input type="text" class="form-control" name="formalitie_name" id="formalitie_name" placeholder="Nombre">
            
        </div>
        <div style="margin-bottom: 10px;" class="form-floating">
            <input type="text" class="form-control" name="description" id="description" placeholder="Descripción">
            
        </div>
        <div style="margin-bottom: 10px;" class="form-floating">
            <input type="text" class="form-control" name="cost" id="cost" placeholder="Costo">
            
        </div>
        <center>
        <button style="margin-bottom: 5px; margin-top: 15px; background: #4B6587; font-size:20px" type="submit" class="btn btn-primary">Registrar  Trámite</button>
        </center>
    </form>  
<!-- FIN -->
<?php
    require_once 'parte_inferior.php'
?>
