<?php
    $user = $this->d['user'];
    require_once 'parte_superior.php'
?>
<!-- inicio del contenido principal -->
<div class="container">
    <h1>Registrar Encargado de Dept</h1>
    <form style="margin-left: 50px; margin-right:50px" method="POST" action="<?php echo constant('URL'); ?>admin/newManager" name="register-form">
        
        <div style="margin-top: 80px; margin-bottom: 10px;" class="form-floating">
            <input type="text" class="form-control" name="manager_name" id="manager_name" placeholder="Nombre del encargado">
            
        </div>
        <div style="margin-bottom: 10px;" class="form-floating">
            <input type="text" class="form-control" name="manager_surname" id="manager_surname" placeholder="Apellido">
            
        </div>
        <center>
        <button style="margin-bottom: 5px; margin-top: 15px; background: #4B6587; font-size:15px" type="submit" class="btn btn-primary">Registrar nuevo encargado</button>           
        </center>
    </form>  
</div>

<!-- FIN -->
<?php
    require_once 'parte_inferior.php'
?>
