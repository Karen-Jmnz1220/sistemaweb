<?php
    $user = $this->d['user'];
    require_once 'parte_superior.php'
?>
<!-- inicio del contenido principal -->
<div class="container">
    <h1>Registrar nuevo departamento</h1>
    <form style="margin-left: 50px; margin-right:50px" method="POST" action="<?php echo constant('URL'); ?>admin/newDept" name="register-form">
        
        <div style="margin-top: 80px; margin-bottom: 10px;" class="form-floating">
            <input type="text" class="form-control" name="name_dept" id="name_dept" placeholder="Nombre" required>
            
        </div>
        <div style="margin-bottom: 10px;" class="form-floating">
            <input type="text" class="form-control" name="idManager" id="idManager" placeholder="Id del encargado" required>
         
        </div>
        <div style="margin-bottom: 10px;" class="form-floating">
            <input type="text" class="form-control" name="address" id="address" placeholder="Dirección" required>
            
        </div>
        <div style="margin-bottom: 10px;" class="form-floating">
            <input type="text" class="form-control" name="phone" id="phone" placeholder="Telefono" required>
            
        </div>
        <div style="margin-bottom: 10px;" class="form-floating">
            <input type="email" class="form-control" name="email" id="email" placeholder="Correo electronico" required>
            
        </div>
        <center>
        <button style="margin-bottom: 5px; margin-top: 15px; background: #4B6587; font-size:15px" type="submit" class="btn btn-primary">Registrar nuevo departamento</button>           
        </center>
    </form>  
</div>


<!-- FIN -->
<?php
    require_once 'parte_inferior.php'
?>
