<?php
     $user = $this->d['user'];
    require_once 'parte_superior.php'
?>
<!-- inicio del contenido principal -->
<div class="container">

        <div class="row">
                <div class="col-lg-12">
                    <div class="table-responsive">        
                        <table id="tabla" class="table table-striped table-bordered table-condensed" style="width:100%">
                        <thead class="text-center">
                            <tr style="color:blue;">
                                <th>ID</th>
                                <th>NOMBRE_DEPT</th>
                                <th>ID_MANAGER</th>                                
                                <th>DIRECCION</th>  
                                <th>TELEFONO</th>
                                <th>CORREO ELECTRONICO</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            include_once 'models/deptmodel.php';             
                                foreach($this->departments as $array){
                                    $department = new DepartmentsModel();                                                   
                                    $department = $array;
                                
                            ?>
                            <tr>
                                <td><?php echo $department->getIdDept(); ?></td>
                                <td><?php echo $department->getName_dept(); ?></td>
                                <td><?php echo $department->getIdManager(); ?></td>
                                <td><?php echo $department->getAddress(); ?></td>
                                <td><?php echo $department->getPhone(); ?></td>
                                <td><?php echo $department->getEmail(); ?></td>
                            </tr>
                            <?php
                                }
                            ?>                                
                        </tbody>        
                       </table>                    
                    </div>
                </div>
        </div>  
</div>    

<!-- FIN -->
<?php
    require_once 'parte_inferior.php'
?>