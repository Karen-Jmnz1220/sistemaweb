<?php
     $user = $this->d['user'];
    require_once 'parte_superior.php'
?>
<!-- inicio del contenido principal -->
<div class="container">

        <div class="row">
                <div class="col-lg-12">
                    <div class="table-responsive">        
                        <table id="tabla" class="table table-striped table-bordered table-condensed" style="width:100%">
                        <thead class="text-center">
                            <tr style="color:blue;">
                                <th>ID</th>
                                <th>NOMBRE_ENCARGADO</th>
                                <th>APELLIDO</th>                                
                                
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            include_once 'models/managermodel.php';             
                                foreach($this->manager as $array){
                                    $manager = new ManagerModel();                                                   
                                    $manager = $array;
                                
                            ?>
                            <tr>
                                <td><?php echo $manager->getIdManager(); ?></td>
                                <td><?php echo $manager->getManager_name(); ?></td>
                                <td><?php echo $manager->getManager_surname(); ?></td>
                            </tr>
                            <?php
                                }
                            ?>                                
                        </tbody>           
                       </table>                    
                    </div>
                </div>
        </div>  
</div>    

<!-- FIN -->
<?php
    require_once 'parte_inferior.php'
?>
 