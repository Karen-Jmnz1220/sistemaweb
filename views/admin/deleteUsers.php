<?php
     $user = $this->d['user'];
    require_once 'parte_superior.php'
?>
<!-- inicio del contenido principal -->
<div class="container">

    <div class="row">
        <div class="col-lg-12">
            <div class="table-responsive">        
                <table id="tablaPersonas" class="table table-striped table-bordered table-condensed" style="width:100%">
                    <thead class="text-center">
                        <tr style="color:blue;">
                            <th data-sort="idUser">ID</th>
                            <th data-sort="name">NOMBRE</th>
                            <th data-sort="surname">APELLIDO</th>                                
                            <th data-sort="username">USUARIO</th>  
                            <th data-sort="password">CONTRASEÑA</th>
                            <th data-sort="rol_idRol">ROL</th>
                            <th>Acciones</th>
                        </tr>
                    </thead>
                    <tbody id="databody">
                                               
                    </tbody>           
                </table>                    
            </div>
        </div>
    </div>
</div> 

<script>
    var data = [];
    var copydata = [];
    const sorts = document.querySelectorAll('th');

    async function getData(){
        data = await fetch('http://localhost/sistemaweb//admin/getHistoryJSON')
        .then(res =>res.json())
        .then(json => json);
        this.copydata = [...this.data];
        console.table(data);
        renderData(data);
    }
    getData();

    function renderData(data){
        var databody = document.querySelector('#databody');
        let total = 0;
        databody.innerHTML = '';
        data.forEach(item => { 
            //total += item.amount;
            databody.innerHTML += `<tr>
                <td>${item.idUser}</td>
                <td>${item.name}</td>
                <td>${item.surname}</td>
                <td>${item.username}</td>
                <td>${item.password}</td>
                <td>${item.rol_idRol}</td>
                <td><a href="http://localhost/sistemaweb/admin/deleteUser/${item.idUser}">Eliminar</a></td>
                </tr>`;
        });
    }
</script> 


<?php
    require_once 'parte_inferior.php'
?>
