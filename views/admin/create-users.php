<?php
     $user = $this->d['user'];
     $role = $this->d['role'];
    require_once 'parte_superior.php'
?>
<!-- inicio del contenido principal -->
<div class="container">
    <h1>Registro de Usuarios</h1>
    <form style="margin-left: 50px; margin-right:50px" method="POST" action="<?php echo constant('URL'); ?>admin/newUser">
        <div><?php (isset($this->errorMessage))?  $this->errorMessage : '' ?></div>
        <div style="margin-top: 70px; margin-bottom: 10px;" class="form-floating">
        <input type="text" class="form-control" name="name" id="name" placeholder="Nombre" required>
           
        </div>
        <div style="margin-bottom: 10px;" class="form-floating">
            <input type="text" class="form-control" name="surname" id="surname" placeholder="Apellido" required>
            
        </div>
        <div style="margin-bottom: 10px;" class="form-floating">
            <input type="text" class="form-control" name="username" id="username" placeholder="Usuario" required>
           
        </div>
        <div style="margin-bottom: 10px;" class="form-floating">
            <input type="password" class="form-control" name="password" id="password" placeholder="Contraseña" required>
          
        </div>
        <div style="margin-bottom: 10px;" class="form-floating">
            <select style="width: 200px;" class="custom-select" aria-label="Default select example" name="rol_idRol" required required>
                <option selected>Seleccione el rol</option>
                <option value="1">Admin</option>
                <option value="2">User</option>
                <option value="3">Manager</option>
            </select>
            <!-- <input type="number" class="form-control" name="rol_idRol" id="rol_idRol" placeholder="rol_idRol"> -->
      
        </div>
        <center>
            <button style="margin-bottom: 5px; margin-top: 15px; background: #4B6587; font-size:20px" type="submit" class="btn btn-primary">Registrar  Ususario</button>
        </center>  
    </form>  
<!-- FIN -->
<?php
    require_once 'parte_inferior.php'
?>
