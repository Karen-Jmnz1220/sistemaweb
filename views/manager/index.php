<?php
    $user = $this->d['user'];
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>Dashboard</title>
        
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
        <link href="<?php echo constant('URL'); ?>resource/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
        <link href="<?php echo constant('URL'); ?>resource/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
        <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@300&display=swap" rel="stylesheet">

         <!--datables CSS básico-->
        <link rel="stylesheet" type="text/css" href="<?php echo constant('URL'); ?>resource/vendor/datatables/datatables.min.css"/>
        <!--datables estilo bootstrap 4 CSS-->  
        <link rel="stylesheet"  type="text/css" href="<?php echo constant('URL'); ?>resource/vendor/datatables/DataTables-1.10.18/css/dataTables.bootstrap4.min.css">
        <link rel="stylesheet" href="<?php echo constant('URL'); ?>resource/css/formaliti.css">   
    </head>
    <body>
        <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <div class="container-fluid">
                <div class="collapse navbar-collapse" id="navbarNavDropdown">
                    <ul class="navbar-nav">
                        <li style="margin-left: 1350px;" class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle"  id="navbarDropdownMenuLink" role="button" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <div class="name"><h4><?php echo $user->getName(); ?></h4></div>
                                <div class="photo"><h5>
                                    <?php  if($user->getPhoto() == ''){?>
                                            <i style="size: 20px;" class="far fa-user-circle"></i>
                                        <?php }else{ ?>
                                            <img src="<?php echo constant('URL'); ?>public/img/photos/<?php echo $user->getPhoto() ?>" width="32" />
                                        <?php }  ?></h5>
                                </div>
                            </a>
                        
                            <ul  class="dropdown-menu dropdown-menu-right shadow animated--grow-in" >
                                <a class="dropdown-item" data-bs-toggle="modal" data-bs-target="#logoutModal">
                                    <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>
                                    Logout
                                </a>
                            </ul> 
                        </li>
                    </ul>
                </div>
            </div>
        </nav>

        <!-- MultiStep Form -->
        <div id="main-container">
            <div  id="user-container" class="container">
                <h4>Tramites iniciados</h4>
                <table id="tabla" class="table table-striped table-bordered table-condensed" style="width:100%">
                    <thead class="text-center">
                        <tr style="color:blue;">
                            <th >ID</th>
                            <th>NOMBRE_TRAMITE</th>
                            <th>NOMBRE</th>                                
                            <th>APELLIDO</th>  
                            <th>DEPARTAMENTO</th>
                            <th>PASO EN CURSO</th>
                            <th>ESTATUS</th>
                            <th>ANOTACION</th>
                            <th>Acción</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                                   
                                foreach($this->procedures as $array){
                                    $procedures = new ProceduresModel();                                                   
                                    $procedure = $array;                    
                        ?>
                        <tr>
                            <td><?php echo $procedure->getIdProcedure(); ?></td>
                            <td><?php echo $procedure->getFormalitie_name(); ?></td>
                            <td><?php echo $procedure->getName(); ?></td>
                            <td><?php echo $procedure->getSurname(); ?></td>
                            <td><?php echo $procedure->getName_dept(); ?></td>
                            <td><?php echo $procedure->getStep_name(); ?></td>
                            <td><?php echo $procedure->getStatus(); ?></td>
                            <td><?php echo $procedure->getAnnotation(); ?></td>
                            <td><a class='btn btn-link btnEditar' tabindex='-1' role='button' aria-disabled='true'>Editar</a></td>
                        </tr>
                        <?php
                        }
                        ?>                                
                    </tbody>           
                </table>

                <h4>Tramites con documentación pendiente de aprobar</h4>
                <table id="tabla" class="table table-striped table-bordered table-condensed" style="width:100%">
                    <thead class="text-center">
                        <tr style="color:blue;">
                            <th >ID</th>
                            <th>Imagen</th>
                            <th>Usuario</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                                   
                                foreach($this->uploadImg as $array){
                                    $uploadImg = new UploadImgModel();                                                   
                                    $upload = $array;                    
                        ?>
                        <tr>
                            <td><?php echo $upload->getIdImage(); ?></td>
                            <td><?php echo $upload->getImage(); ?></td>
                            <td><?php echo $upload->getIdUser(); ?></td>
                            <td><a class='btn btn-link btnEditar' tabindex='-1' role='button' aria-disabled='true'>Editar</a></td>
                        </tr>
                        <?php
                        }
                        ?>                                
                    </tbody>           
                </table>
            </div>
        </div>

        <div class="modal" tabindex="-1" id="logoutModal">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">¿Deseas cerrar sesión?</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        <p>Seleccione "Cerrar sesión" a continuación si está listo para finalizar su sesión actual.</p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cancelar</button>
                        <a class="btn btn-primary" href="<?php echo constant('URL'); ?>logout">Cerrar Sesión</a>
                    </div>
                </div>
            </div>
        </div>
       

        <!-- Bootstrap core JavaScript-->
        <script src="<?php echo constant('URL'); ?>resource/vendor/jquery/jquery.min.js"></script>
        
        <!-- Core plugin JavaScript-->
        <script src="<?php echo constant('URL'); ?>resource/vendor/jquery-easing/jquery.easing.min.js"></script>

        <!-- datatables JS -->
        <script type="text/javascript" src="<?php echo constant('URL'); ?>resource/vendor/datatables/datatables.min.js"></script>
   
        <script src="<?php echo constant('URL'); ?>resource/js/progressStep.js"></script>
           
    </body>
</html>