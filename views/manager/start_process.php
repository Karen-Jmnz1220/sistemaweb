<?php
    $user = $this->d['user'];
    
    require_once 'parte_superior.php';
?>
<!-- inicio del contenido principal -->
<div class="container">
    <div class="row">
        <div class="col-lg-12">
            <div class="table-responsive">        
                <table id="tabla" class="table table-striped table-bordered table-condensed" style="width:100%">
                    <thead class="text-center">
                        <tr style="color:blue;">
                            <th >ID</th>
                            <th>NOMBRE_TRAMITE</th>
                            <th>NOMBRE</th>                                
                            <th>APELLIDO</th>  
                            <th>DEPARTAMENTO</th>
                            <th>ID MANAGER</th>
                            <th>PASO EN CURSO</th>
                            <th>ESTATUS</th>
                            <th>ANOTACION</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                                   
                                foreach($this->procedures as $array){
                                    $procedures = new ProceduresModel();                                                   
                                    $procedure = $array;                    
                        ?>
                        <tr>
                            <td><?php echo $procedure->getIdProcedure(); ?></td>
                            <td><?php echo $procedure->getFormalitie_name(); ?></td>
                            <td><?php echo $procedure->getName(); ?></td>
                            <td><?php echo $procedure->getUsername(); ?></td>
                            <td><?php echo $procedure->getName_dept(); ?></td>
                            <td><?php echo $procedure->getIdManager(); ?></td>
                            <td><?php echo $procedure->getStep_name(); ?></td>
                            <td><?php echo $procedure->getStatus(); ?></td>
                            <td><?php echo $procedure->getAnnotation(); ?></td>
                        </tr>
                        <?php
                        }
                        ?>                                
                    </tbody>           
                </table>                    
            </div>
        </div>
    </div>  
</div> 

<!-- FIN -->
<?php
    require_once 'parte_inferior.php';
?>