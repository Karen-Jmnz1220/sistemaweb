<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <link rel="stylesheet" href="<?php echo constant('URL'); ?>resource/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo constant('URL'); ?>resource/css/formaliti.css">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <title>Tramites</title>
</head>
<style>
    .u1st .accessibilityClass {
        position: fixed !important;
        top: 55px;
        left: -50px;
        transform: rotate(90deg);
        position: absolute !important;
       top: expression(document.compatMode && document.compatMode = 'CSS1Compat' ? documentElement.scrollTop : document.body.scrollTop);
        z-index: 1000000;
        cursor: pointer;
        text-align: center;
        padding: 3px 0;
        border-radius: 4px;
        font: bold 15px Arial !important;
        background:  #F39200 !important;
        color #3B3835 !important;
        padding: 7px 20px 7px 20px;

    }
</style>
<body>
<ul class="wizard-steps">
		<li class="completed">
			<h5 aria-hidden="true" role="document" aria-label="Para hacer este sitio web accesible al lector de pantalla, Oprima alt + 1. Para dejar de recibir este mensaje, oprima alt + 2">Paso 1</h5> 
			<span>Iniciar trámite</span>
		</li>
		
		
		<li class="">
			<h5 aria-hidden="true" role="document" aria-label="Para hacer este sitio web accesible al lector de pantalla, Oprima alt + 1. Para dejar de recibir este mensaje, oprima alt + 2">Paso 2</h5> 
			<span>Ingresar informacioón</span>
		</li>
		<li class="">
			<h5 aria-hidden="true" role="document" aria-label="Para hacer este sitio web accesible al lector de pantalla, Oprima alt + 1. Para dejar de recibir este mensaje, oprima alt + 2">Paso 3</h5> 
			<span>Finalizar trámite</span>
		</li>
		<li class=""><i class="glyphicon glyphicon-ok"></i></li>
		
	</ul>
		
		<div>
			Para iniciar el proceso de tramite debes tener a la mano los siguientes papeles:<br>
			<ul>
                <li>Acta de Nacimiento</li>
				<li>CURP</li>
                <li>Recibo de luz</li>
                <li>Recibo de Pago </li>
				<li>Correo electrónico</li>
			</ul>
			
		</div>

    <!-- Optional JavaScript -->
  <!-- jQuery first, then Popper.js, then Bootstrap JS -->
  <script src="resource/js/jquery-3.4.1.min.js"></script>
  <!-- <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script> -->
  <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
    integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous">
  </script>
  <script src="resource/js/bootstrap.min.js"></script>
  <!-- <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script> -->
   

    
</body>
</html>TYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <link rel="stylesheet" href="<?php echo constant('URL'); ?>resource/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo constant('URL'); ?>resource/css/formalitie.css">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <title>Tramites</title>
</head>
<body>
<ul class="wizard-steps">
		<li class="completed">
			<h5 aria-hidden="true" role="document" aria-label="Para hacer este sitio web accesible al lector de pantalla, Oprima alt + 1. Para dejar de recibir este mensaje, oprima alt + 2">Paso 1</h5> 
			<span>Iniciar trámite</span>
		</li>
		
		
		<li class="">
			<h5 aria-hidden="true" role="document" aria-label="Para hacer este sitio web accesible al lector de pantalla, Oprima alt + 1. Para dejar de recibir este mensaje, oprima alt + 2">Paso 2</h5> 
			<span>Ingresar informacioón</span>
		</li>
		<li class="">
			<h5 aria-hidden="true" role="document" aria-label="Para hacer este sitio web accesible al lector de pantalla, Oprima alt + 1. Para dejar de recibir este mensaje, oprima alt + 2">Paso 3</h5> 
			<span>Finalizar trámite</span>
		</li>
		<li class=""><i class="glyphicon glyphicon-ok"></i></li>
		
	</ul>
		
		<div>
			Para iniciar el proceso de tramite debes tener a la mano los siguientes papeles:<br>
			<ul>
                <li>Acta de Nacimiento</li>
				<li>CURP</li>
                <li>Recibo de luz</li>
                <li>Recibo de Pago </li>
				<li>Correo electrónico</li>
			</ul>
			
		</div>

    <!-- Optional JavaScript -->
  <!-- jQuery first, then Popper.js, then Bootstrap JS -->
  <script src="resource/js/jquery-3.4.1.min.js"></script>
  <!-- <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script> -->
  <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
    integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous">
  </script>
  <script src="resource/js/bootstrap.min.js"></script>
  <!-- <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script> -->
   

    
</body>
</html>