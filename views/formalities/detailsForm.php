<?php
    $formalities = $this->d['formalities'];
    $form = $this->d['form'];
    $found = $this->d['found'];
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@300&display=swap" rel="stylesheet">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="<?php echo constant('URL'); ?>resource/css/formalitie.css">
    
    <title>Tramites</title>
</head>
<body>
    <div class="card">
        <div class="card-header">
            <h2 style="text-align: center;">
                <?php
                               
                    foreach($this->formalities as $array){
                        $formalitie = new FormalitiesModel();                                                   
                        $formalitie = $array;                
                    
                        echo $formalitie->getFormalitie_name(); 
                    }
                ?>
            </h2>
        </div>
        <div class="card-body">
            <section id="legalFound-formalitie-details">
                <h5>FUNDAMENTOS JURIDICOS</h5>
                <table id="tabla" class="table table-striped table-bordered table-condensed" style="width:100%">
                    <thead class="text-center">
                        <tr style="color:blue;">                      
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                                         
                            foreach($this->found as $array){
                                $foundation = new LegalFoundationsModel();                                                   
                                $foundation = $array; 
                               
                        ?>
                        <tr>
                            <td><?php echo $foundation->getDescription() ?></td>
                        </tr>
                        <?php
                            }
                        ?>                                
                    </tbody>           
                </table>    
            </section>

            <hr class="sidebar-divider my-0">

            <section id="description-formalitie-details">
                <h5 style="margin-top: 30px">DESCRIPCIÓN</h5>
                <table id="tabla" class="table table-striped table-bordered table-condensed" style="width:100%">
                    <thead class="text-center">
                        <tr style="color:blue;">                      
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                                        
                            foreach($this->formalities as $array){
                                $formalitie = new FormalitiesModel();                                                   
                                $formalitie = $array;
                            
                        ?>
                        <tr>
                            <td><?php echo $formalitie->getDescription()?></td>
                        </tr>
                        <?php
                            }
                        ?>                                
                    </tbody>           
                </table> 
            </section>

            <hr class="sidebar-divider my-0">

            <section id="pasos-formalitie-details">
                <h5 style="margin-top: 30px">MODALIDAD</h5>
                <div class="row">
                    <div class="col-sm-5 col-md-6">
                        <table id="tabla" class="table table-striped table-bordered table-condensed" style="width:100%">
                            <thead class="text-center">
                                <tr style="color:blue;">  
                                    <th scope="col">#</th>
                                    <th scope="col">PASOS PRESENCIALES</th>                    
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <th scope="row">1</th>
                                    <td>ACUDIR A OFICINAS</td>
                                </tr>
                                <tr>
                                    <th scope="row">2</th>
                                    <td>PRESENTAR DOCUMENTACIÓN</td>
                                </tr>
                                <tr>
                                    <th scope="row">3</th>
                                    <td>ANÁLISIS DE LA INFORMACIÓN</td>
                                </tr>
                                <tr>
                                    <th scope="row">4</th>
                                    <td>REALIZAR PAGO</td>
                                </tr>
                                <tr>
                                    <th scope="row">5</th>
                                    <td>RECIBIR RESPUESTA</td>
                                </tr>                          
                            </tbody>           
                        </table> 
                    </div>
                    <div class="col-sm-5 offset-sm-2 col-md-6 offset-md-0">
                        <table id="tabla" class="table table-striped table-bordered table-condensed" style="width:100%">
                            <thead class="text-center">
                                <tr style="color:blue;">  
                                    <th scope="col">#</th>
                                    <th scope="col">PASOS EN LINEA</th>                   
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <th scope="row">1</th>
                                    <td>INICIAR TRÁMITE</td>
                                </tr>
                                <tr>
                                    <th scope="row">2</th>
                                    <td>SUBIR DOCUMENTACIÓN</td>
                                </tr>
                                <tr>
                                    <th scope="row">3</th>
                                    <td>ANÁLISIS DE LA INFORMACIÓN</td>
                                </tr>
                                <tr>
                                    <th scope="row">4</th>
                                    <td>REALIZAR PAGO</td>
                                </tr>
                                <tr>
                                    <th scope="row">5</th>
                                    <td>RECIBIR RESPUESTA</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div> 
            </section>

            <hr class="sidebar-divider my-0">

            <section id="requisitos-formalitie-details">
                <h5 style="margin-top: 30px">REQUISITOS</h5>
                <table id="tabla" class="table table-striped table-bordered table-condensed" style="width:100%">
                    <thead class="text-center">
                        <tr style="color:blue;">
                            <th>REQUISITO</th>
                            <th>DESCRIPCIÓN DETALLADA</th>
                        </tr>
                    </thead>
                   <tbody>
                        <?php
                                
                            foreach($this->form as $array){
                                $for = new FormalitiesModel();                                                   
                                $form = $array;                        
                        ?>
                        <tr>
                            <td><?php echo $form->getRequirement_name(); ?></td>
                            <td><?php echo $form->getRequirement_descrip(); ?></td>        
                        </tr>
                        <?php
                            }
                        ?>                                
                    </tbody>        
                </table>  
            </section>


            <section id="comenzar-formalitie-details">
                <center>
                <a class="btn btn-primary" href="<?php echo constant('URL'); ?>login" role="button">Iniciar Tramite</a>
                </center>
                <a href="<?php echo constant('URL'); ?>logout/return" class="btn btn-link" tabindex="-1" role="button" aria-disabled="true">Volver</a>
            </section>
            
           

        </div>
    </div>
    
</body>
</html>