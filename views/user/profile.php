<?php
    $user = $this->d['user'];

?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>Drawing Tests</title>
        
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
        <link href="<?php echo constant('URL'); ?>resource/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
        <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@300&display=swap" rel="stylesheet">
        
        <link rel="stylesheet" href="<?php echo constant('URL'); ?>resource/css/user.css">   
    </head>
    <body>
        <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <div class="container-fluid">
                <div class="collapse navbar-collapse" id="navbarNavDropdown">
                    <ul class="navbar-nav">
                        <li style="margin-left: 1390px;" class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="<?php echo constant('URL');?>user" id="navbarDropdownMenuLink" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                                <div class="name"><h5><?php echo $user->getName(); ?></h5></div>
                                <div class="photo"><h5>
                                    <?php  if($user->getPhoto() == ''){?>
                                        <i style="size: 20px;" class="far fa-user-circle"></i>
                                    <?php }else{ ?>
                                        <img src="<?php echo constant('URL'); ?>public/img/photos/<?php echo $user->getPhoto() ?>" width="32" />
                                    <?php }  ?></h5>
                                </div>
                            </a>
                        
                            <ul  class="dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="userDropdown">
                                <li><a class="dropdown-item" href="<?php echo constant('URL'); ?>/user/showProfilePage">Perfil</a></li>
                                <li>
                                    <a class="dropdown-item" data-bs-toggle="modal" data-bs-target="#logoutModal">
                                        Logout
                                    </a>
                                </li>
                                
                            </ul> 
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
        <!-- LogoutModal -->
        <div class="modal" tabindex="-1" id="logoutModal">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">¿Deseas cerrar sesión?</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        <p>Seleccione "Cerrar sesión" a continuación si está listo para finalizar su sesión actual.</p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cancelar</button>
                        <a class="btn btn-primary" href="<?php echo constant('URL'); ?>logout">Cerrar Sesión</a>
                    </div>
                </div>
            </div>
        </div>

        <div id="main-container">
        <?php $this->showMessages();?>
            <div id="user-container" class="container">
                <div id="side-menu">
                    <ul>
                        <li><a href="#info-user-container">Personalizar usuario</a></li>
                        <li><a href="#username-user-container">Modificar Usuario</a></li>
                        <li><a href="#password-user-container">Modificar Contraseña</a></li>     
                    </ul>
                </div>

                <div id="user-section-container">
                    
                    <section id="info-user-container">
                        <form action=<?php echo constant('URL'). 'user/updateName' ?> method="POST">
                            <div class="section">
                                <label style="padding-bottom: 5px;" for="name">Nombre</label><br>
                                <input style="width: 350px" type="text" name="name" id="name" autocomplete="off" required value="<?php echo $user->getName() ?>">
                                <div style="padding-top: 10px; "><input type="submit" value="Cambiar nombre" /></div>
                            </div>
                        </form>

                        <form action="<?php echo constant('URL'). 'user/updatePhoto' ?>" method="POST" enctype="multipart/form-data">
                            <div class="section">
                                <label style="padding-bottom: 5px;" for="photo">Foto de perfil</label>        
                                    <?php
                                        if(!empty($user->getPhoto())){
                                    ?>
                                        <img src="<?php echo constant('URL') ?>public/img/photos/<?php echo $user->getPhoto() ?>" width="50" height="50" />
                                    <?php
                                    }
                                    ?>
                                    <input style="width: 350px" type="file" name="photo" id="photo" autocomplete="off" required>
                                    <div style="padding-top: 10px;" ><input type="submit" value="Cambiar foto de perfil" /></div>
                            </div>
                        </form>
                    </section>

                    <section id="username-user-container">
                        <form action=<?php echo constant('URL'). 'user/updateUsername' ?> method="POST">
                            <div class="section">
                                <label style="padding-bottom: 5px;" for="name">Usuario</label><br>
                                <input style="width: 350px" type="text" name="username" id="username" autocomplete="off" required value="<?php echo $user->getusername() ?>">
                                <div style="padding-top: 10px; "><input type="submit" value="Cambiar usuario" /></div>
                            </div>
                        </form>
                    </section>

                    <section id="password-user-container">
                        <form action="<?php echo constant('URL'). 'user/updatePassword' ?>" method="POST">
                            <div class="section">
                                <label style="padding-bottom: 5px;" for="current_password">Password actual</label><br>
                                <input style="width: 350px" type="password" name="current_password" id="current_password" autocomplete="off" required>

                                <label style="padding-bottom: 5px; padding-top: 10px;" for="new_password">Nuevo password</label> <br>
                                <input style="width: 350px" type="password" name="new_password" id="new_password" autocomplete="off" required>
                                <div style="padding-top: 10px;"><input type="submit" value="Cambiar password" /></div>
                            </div>
                        </form>
                    </section>
                </div><!-- user section container -->
            </div><!-- user container -->
        </div><!-- main container -->

        <script src="<?php echo constant('URL'); ?>resource/vendor/jquery/jquery.min.js"></script>
        <script src="<?php echo constant('URL'); ?>resource/js/profile.js"></script>
                                
    </body>
</html>

