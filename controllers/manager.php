<?php

class Manager extends SessionController{
    
    function __construct(){
        parent::__construct();

        $this->user = $this->getUserSessionData();
        error_log("user " . $this->user->getName());
    }

    function render(){
        $idManager = $this->user->getIdUser();
        $proceduresModel = new ProceduresModel();
        $procedures = $proceduresModel->get($idManager);
        $this->view->procedures = $procedures;

        $uploadImgModel = new UploadImgModel();
        $uploadImg = $uploadImgModel->get($idManager);
        $this->view->uploadImg = $uploadImg;
        
        $this->view->render('manager/index', [
            "user" => $this->user
        ]);
    }
    function showUploadDoc(){
        $idManager = $this->user->getIdUser();
        $uploadImgModel = new UploadImgModel();
        $uploadImg = $uploadImgModel->get($idManager);
        $this->view->uploadImg = $uploadImg;
        $this->view->render('manager/start_process',[
            "user" => $this->user
        ]);
    }
    
}
?>