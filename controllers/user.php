
<?php

class User extends SessionController{

    private $user;

    function __construct(){
        parent::__construct();

        $this->user = $this->getUserSessionData();
        $idUser= $this->user->getIdUser();
        error_log("user " . $this->user->getName());
        error_log("IdUser " . $idUser);
    }

    function render(){
        $this->view->render('user/profile', [
            "user" => $this->user
        ]);
    }

    function updateName(){
        $idUser= $this->user->getIdUser();
        if(!$this->existPOST('name')){
            $this->redirect('user', ['error' => Errors::ERROR_USER_UPDATENAME]);
            return;
        }

        $name = $this->getPost('name');

        if(empty($name)){
            $this->redirect('user/showProfilePage', ['error' => Errors::ERROR_USER_UPDATENAME]);
            return;
        }
        
        $this->user->setName($name);
        if($this->user->updateName($name, $idUser)){
            $this->redirect('user/showProfilePage', ['success' => Success::SUCCESS_USER_UPDATENAME]);
        }else{
            //error
        }
    }
    function updateUsername(){
        $idUser= $this->user->getIdUser();
        if(!$this->existPOST('username')){
            $this->redirect('user/showProfilePage', ['error' => Errors::ERROR_USER_UPDATEUSERNAME]);
            return;
        }

        $username = $this->getPost('username');

        if(empty($username)){
            $this->redirect('user/showProfilePage', ['error' => Errors::ERROR_USER_UPDATEUSERNAME]);
            return;
        }
        
        $this->user->setName($username);
        if($this->user->updateUsername($username, $idUser)){
            $this->redirect('user/showProfilePage', ['success' => Success::SUCCESS_USER_UPDATEUSERNAME]);
        }else{
            //error
        }
    }

    function updatePassword(){
        $idUser= $this->user->getIdUser();
        if(!$this->existPOST(['current_password', 'new_password'])){
            $this->redirect('user/showProfilePage', ['error' => Errors::ERROR_USER_UPDATEPASSWORD]);
            return;
        }
        $current = $this->getPost('current_password');
        $new     = $this->getPost('new_password');

        if(empty($current) || empty($new)){
            $this->redirect('use/showProfilePager', ['error' => Errors::ERROR_USER_UPDATEPASSWORD_EMPTY]);
            return;
        }
        if($current === $new){
            $this->redirect('user/showProfilePage', ['error' => Errors::ERROR_USER_UPDATEPASSWORD_ISNOTTHESAME]);
            return;
        }
        //validar que el current es el mismo que el guardado
        $newHash = $this->model->comparePasswords($current, $this->user->getIdUser());
        if($newHash != NULL){
            $this->user->setPassword($new, true);
            
            if($this->user->updatePassword($new, $idUser)){
                $this->redirect('user/showProfilePage', ['success' => Success::SUCCESS_USER_UPDATEPASSWORD]);
            }else{
                $this->redirect('user/showProfilePage', ['error' => Errors::ERROR_USER_UPDATEPASSWORD]);
            }
        }else{
            $this->redirect('user/showProfilePage', ['error' => Errors::ERROR_USER_UPDATEPASSWORD]);
            return;
        }
    }

    function updatePhoto(){
        if(!isset($_FILES['photo'])){
            $this->redirect('user', ['error' => Errors::ERROR_USER_UPDATEPHOTO]);
            return;
        }
        $photo = $_FILES['photo'];
        $target_dir = "public/img/photos/";
        $extarr = explode('.',$photo["name"]);
        $filename = $extarr[sizeof($extarr)-2];
        $ext = $extarr[sizeof($extarr)-1];
        $hash = md5(Date('Ymdgi') . $filename) . '.' . $ext;
        $target_file = $target_dir . $hash;
        $uploadOk = 1;
        $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
        
        $check = getimagesize($photo["tmp_name"]);
        if($check !== false) {
            $uploadOk = 1;
        } else {
            $uploadOk = 0;
        }
        if ($uploadOk == 0) {
            $this->redirect('user', ['error' => Errors::ERROR_USER_UPDATEPHOTO_FORMAT]);
        } else {
            if (move_uploaded_file($photo["tmp_name"], $target_file)) {
                $this->model->updatePhoto($hash, $this->user->getIdUser());
                $this->redirect('user', ['success' => Success::SUCCESS_USER_UPDATEPHOTO]);
            } else {
                $this->redirect('user', ['error' => Errors::ERROR_USER_UPDATEPHOTO]);
            }
        } 
    }

    
}

?>