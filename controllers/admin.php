<?php

class Admin extends SessionController{
    
    function __construct(){
        parent::__construct();

        $this->user = $this->getUserSessionData();
        error_log("user " . $this->user->getName());
    }

    function render(){
        $this->view->render('admin/index', [
            "user" => $this->user
        ]);
    }
    function showUsers(){
        $usersModel = new UserModel();
        $users = $usersModel->getAll();
        $this->view->users = $users;
        $this->view->render('admin/showUsers',[
            "user" => $this->user
        ]);
    }
    function showDept(){
        $deptModel = new DepartmentsModel();
        $departments = $deptModel->getAll();
        $this->view->departments = $departments;
        $this->view->render('admin/showDept',[
            "user" => $this->user
        ]);
    }
    function showForm(){
        $formModel = new FormalitiesModel();
        $formalities = $formModel->getAll();
        $this->view->formalities = $formalities;
        $this->view->render('admin/showFormalities',[
            "user" => $this->user
        ]);
    }

    function getHistoryJSON(){
        header('Content-Type: application/json');
        $res = [];
        $usersModel = new UserModel();
        $users = $usersModel->getAll();
    
        foreach ($users as $user) {
            array_push($res, $user->toArray());
        }
        
        echo json_encode($res);

    }
    function getHistoryJSONDepts(){
        header('Content-Type: application/json');
        $res = [];
        $deptModel = new DepartmentsModel();
        $departments = $deptModel->getAll();
    
        foreach ($departments as $dept) {
            array_push($res, $dept->toArray());
        }
        
        echo json_encode($res);

    }
    function getHistoryJSONFormalities(){
        header('Content-Type: application/json');
        $res = [];
        $formalitiesModel = new FormalitiesModel();
        $formalities = $formalitiesModel->getAll();
    
        foreach ($formalities as $form) {
            array_push($res, $form->toArray());
        }
        
        echo json_encode($res);

    }
    function deleteUsers(){
        $this->view->render('admin/deleteUsers', [
            "user" => $this->user
        ]);
    }

    function deleteDepts(){
        $this->view->render('admin/deleteDepts', [
            "user" => $this->user
        ]);
    }

    function deleteFormalities(){
        $this->view->render('admin/deleteFormalitie', [
            "user" => $this->user
        ]);
    }
    function updateUsers(){
        $usersModel = new UserModel();
        $users = $usersModel->getAll();
        $this->view->users = $users;
        $this->view->render('admin/update-users', [
            "user" => $this->user
        ]);
    }
    function updateDepts(){
        $deptModel = new DepartmentsModel();
        $departments = $deptModel->getAll();
        $this->view->departments = $departments;

        $manager = new UserModel();
        $this->view->render('admin/update-depts', [
            "user" => $this->user,
            'manager' => $manager->getAll(),
        ]);
    }
    function updateFormalities(){
        $formModel = new FormalitiesModel();
        $formalities = $formModel->getAll();
        $this->view->formalities = $formalities;
        $this->view->render('admin/update-formalitie', [
            "user" => $this->user
        ]);
    }
    function deleteUser($params){
        error_log("Admin::delete()");
        
        if($params === NULL) $this->redirect('admin', ['error' => Errors::ERROR_ADMIN_DELETE]);
        $idUser = $params[0];
        error_log("Admin::delete() idUser = " . $idUser);
        $usersModel = new UserModel();
        $res = $usersModel->delete($idUser);

        if($res){
            $this->redirect('admin/deleteUsers', ['success' => Success::SUCCESS_RESOURCE_DELETE]);
        }else{
            $this->redirect('admin/deleteUsers', ['error' => Errors::ERROR_ADMIN_DELETE]);
        }
    }
    function deleteDept($params){
        error_log("Admin::delete()");
        
        if($params === NULL) $this->redirect('admin', ['error' => Errors::ERROR_ADMIN_DELETE]);
        $idDept = $params[0];
        error_log("Admin::delete() idDept = " . $idDept);
        $departmentsModel = new DepartmentsModel();
        $res = $departmentsModel->delete($idDept);

        if($res){
            $this->redirect('admin/deleteDepts', ['success' => Success::SUCCESS_RESOURCE_DELETE]);
        }else{
            $this->redirect('admin/deleteDepts', ['error' => Errors::ERROR_ADMIN_DELETE]);
        }
    }
    function deleteFormalitie($params){
        error_log("Admin::delete()");
        
        if($params === NULL) $this->redirect('admin', ['error' => Errors::ERROR_ADMIN_DELETE]);
        $idFormalitie = $params[0];
        error_log("Admin::delete() idFormalitie = " . $idFormalitie);
        $formalitieModel = new FormalitiesModel();
        $res = $formalitieModel->delete($idFormalitie);

        if($res){
            $this->redirect('admin/deleteFormalities', ['success' => Success::SUCCESS_RESOURCE_DELETE]);
        }else{
            $this->redirect('admin/deleteFormalities', ['error' => Errors::ERROR_ADMIN_DELETE]);
        }
    }
    function updateUser($params){
        error_log('Admin::updateDept()');
        $idUser = $params[0];
        
        if($this->existPOST(['name', 'surname', 'username', 'password'])){
            $name = $this->getPost('name');
            $surname = $this->getPost('surname');
            $username = $this->getPost('username');
            $password = $this->getPost('password');
                        
            $userModel = new UserModel();
            $userModel->setName($name);
            $userModel->setSurname($surname);
            $userModel->setUsername($username);
            $userModel->setPassword($password);
            
            
            $res = $userModel->updateAll($idUser);
            if($res){
                error_log("Admin::update() idUser = " . $idUser);
                $this->redirect('admin/updateUsers', ['success' => Success::SUCCESS_ADMIN_UPDATEUSER]);
            }else{
                $this->redirect('admin/updateUsers', ['error' => Errors::ERROR_ADMIN_UPDATEUSER_EXISTS]);
            }
        }else{
            
            $this->redirect('admin/updateUsers', ['error' => Errors::ERROR_ADMIN_UPDATEUSER]);
        }
    }
    function updateDept($params){
       
        error_log('Admin::updateDept()');
        $idDept = $params[0];
        
        if($this->existPOST(['name_dept', 'idManager', 'address', 'phone', 'email'])){
            $name_dept = $this->getPost('name_dept');
            $idManager = $this->getPost('idManager');
            $address = $this->getPost('address');
            $phone = $this->getPost('phone');
            $email = $this->getPost('email');

                        
            $departmentsModel = new DepartmentsModel();
            $departmentsModel->setName_dept($name_dept);
                $departmentsModel->setIdManager($idManager);
                $departmentsModel->setAddress($address);
                $departmentsModel->setPhone($phone);
                $departmentsModel->setEmail($email);
            
            $res = $departmentsModel->updateAll($idDept);
            if($res){
                error_log("Admin::update() idDept = " . $idDept);
                $this->redirect('admin/updateDepts', ['success' => Success::SUCCESS_ADMIN_UPDATEDEPT]);
            }else{
                $this->redirect('admin/updateDepts', ['error' => Errors::ERROR_ADMIN_UPDATEDEPT_EXISTS]);
            }
        }else{
            /* $this->errorAtREGISTER('Error al registrar el dept. Inténtalo más tarde');
            return; */
            $this->redirect('admin/updateDepts', ['error' => Errors::ERROR_ADMIN_UPDATEUSER]);
        }
    }
    function updateForms($params){
       
        error_log('Admin::updateForms()');
        $idFormalitie = $params[0];
        
        if($this->existPOST(['formalitie_name', 'description', 'idDept', 'cost'])){
            $formalitie_name = $this->getPost('formalitie_name');
            $description = $this->getPost('description');
            $idDept = $this->getPost('idDept');   
            $cost = $this->getPost('cost');
    
            $formalitiesModel = new FormalitiesModel();
            $formalitiesModel->setFormalitie_name($formalitie_name);
            $formalitiesModel->setDescription($description);
            $formalitiesModel->setIdDept($idDept);
            $formalitiesModel->setCost($cost);
                      
            $res = $formalitiesModel->updateAll($idFormalitie);
            if($res){
                error_log("Admin::update() idFormalities = " . $idFormalitie);
                $this->redirect('admin/updateFormalities', ['success' => Success::SUCCESS_ADMIN_UPDATEFORMALITIE]);
            }else{
                $this->redirect('admin/updateFormalities', ['error' => Errors::ERROR_ADMIN_UPDATEFORMALITIE_EXISTS]);
            }
        }else{
            $this->redirect('admin/updateFormalities', ['error' => Errors::ERROR_ADMIN_UPDATEFORMALITIE]);
        }
    }
  
    function createDept(){
        $this->view->render('admin/create-dept',[
            "user" => $this->user
        ]);
    }
    function createUsers(){
        
        $this->view->render('admin/create-users', [
            "user" => $this->user
        ]);
    }
    function createForm(){
        $this->view->render('admin/create-formalitie', [
            "user" => $this->user
        ]);
    }
 
    function newUser(){
        error_log('Admin::newUser()');
        if($this->existPOST(['name', 'surname', 'username', 'password', 'rol_idRol'])){
            $name = $this->getPost('name');
            $surname = $this->getPost('surname');
            $username = $this->getPost('username');
            $password = $this->getPost('password');
            $rol_idRol = $this->getPost('rol_idRol');
            

                //validate data
                if($name == '' || empty($name) || $surname == '' || empty($surname) ||  $username == '' || empty($username) || $password == '' || empty($password) || $rol_idRol == '' || empty($rol_idRol)){
                
                    $this->redirect('admin/showUsers', ['error' => Errors::ERROR_ADMIN_NEWDUSER_EMPTY]);
                }
    
                $user = new UserModel();
                $user->setName($name);
                $user->setSurname($surname);
                $user->setUsername($username);
                $user->setPassword($password);
                $user->setRol_idRol($rol_idRol);
                                
                if($user->exists($username)){
                  
                    $this->redirect('admin/showUsers', ['error' => Errors::ERROR_ADMIN_NEWUSER_EXISTS]);
                    //return;
                }else if($user->save()){
                   
                    $this->redirect('admin/showUsers', ['success' => Success::SUCCESS_ADMIN_NEWUSER]);
                }else{
                   
                    $this->redirect('admin/showUsers', ['error' => Errors::ERROR_ADMIN_NEWUSERS]);
                }
            }else{
                
                $this->redirect('admin/showUsers', ['error' => Errors::ERROR_ADMIN_NEWUSER_EXISTS]);
            }
    }
    function newDept(){
        error_log('Admin::newDept()');
        if($this->existPOST(['name_dept', 'idManager', 'address', 'phone', 'email'])){
            $name_dept = $this->getPost('name_dept');
            $idManager = $this->getPost('idManager');
            $address = $this->getPost('address');
            $phone = $this->getPost('phone');
            $email = $this->getPost('email');

                //validate data
                if($name_dept == '' || empty($name_dept) || $idManager == '' || empty($idManager) ||  $address == '' || empty($address) || $phone == '' || empty($phone) || $email == '' || empty($email)){
                    // error al validar datos
                    //$this->errorAtREGISTER('Campos vacios');
                    $this->redirect('admin/showDept', ['error' => Errors::ERROR_ADMIN_NEWDEPTS_EMPTY]);
                }
    
                $dept = new DepartmentsModel();
                $dept->setName_dept($name_dept);
                $dept->setIdManager($idManager);
                $dept->setAddress($address);
                $dept->setPhone($phone);
                $dept->setEmail($email);
                                
                if($dept->exists($name_dept)){
                    //$this->errorAtREGISTER('Error al registrar el usuario. Elige un nombre de usuario diferente');
                    $this->redirect('admin/showDept', ['error' => Errors::ERROR_ADMIN_NEWDEPTS_EXISTS]);
                    //return;
                }else if($dept->save()){
                    //$this->view->render('login/index');
                    $this->redirect('admin/showDept', ['success' => Success::SUCCESS_ADMIN_NEWDEPTS]);
                }else{
                    /* $this->errorAtREGISTER('Error al registrar el usuario. Inténtalo más tarde');
                    return; */
                    $this->redirect('admin/showDept', ['error' => Errors::ERROR_ADMIN_NEWDEPTS]);
                }
            }else{
                // error, cargar vista con errores
                //$this->errorAtREGISTER('Ingresa nombre de usuario y password');
                $this->redirect('admin/showDept', ['error' => Errors::ERROR_ADMIN_NEWDEPT_EXISTS]);
            }
    }
    function newFormalitie(){
        error_log('Admin::newFormalitie()');
        if($this->existPOST(['formalitie_name', 'description', 'cost'])){
            $formalitie_name = $this->getPost('formalitie_name');
            $description = $this->getPost('description');
            $cost = $this->getPost('cost');

                if($formalitie_name == '' || empty($formalitie_name) || $description == '' || empty($description) ||  $cost == '' || empty($cost)){
                    
                    $this->redirect('admin/createForm', ['error' => Errors::ERROR_ADMIN_NEWDEPTS_EMPTY]);
                }
    
                $formalitie = new FormalitiesModel();
                $formalitie->setFormalitie_name($formalitie_name);
                $formalitie->setDescription($description);
                $formalitie->setCost($cost);
                                                
                if($formalitie->exists($formalitie_name)){ 
                    $this->redirect('admin/showForm', ['error' => Errors::ERROR_ADMIN_NEWFORMALITIE_EXISTS]);
                }else if($formalitie->save()){
                    $this->redirect('admin/showForm', ['success' => Success::SUCCESS_ADMIN_NEWDFORMALITIE]);
                }else{
                    $this->redirect('admin/showForm', ['error' => Errors::ERROR_ADMIN_NEWFORMALITIE]);
                }
            }else{
                $this->redirect('admin/showForm', ['error' => Errors::ERROR_ADMIN_NEWDEPT_EXISTS]);
            }
    }   
}
?>