<?php

class Register extends SessionController{

    function __construct(){
        parent::__construct();
    }
    function render(){
        $this->view->errorMessage = '';
        $this->view->render('login/register');
    }

    function newUser(){
        if($this->existPOST(['name', 'surname','username', 'password'])){
            
            $name = $this->getPost('name');
            $surname = $this->getPost('surname');
            $username = $this->getPost('username');
            $password = $this->getPost('password');
            
            //Datos validados
            if($name == '' || empty($name) || $surname == '' || empty($surname) ||  $username == '' || empty($username) || $password == '' || empty($password)){
                //Campos vacios
                $this->redirect('register', ['error' => Errors::ERROR_REGISTER_NEWUSER_EMPTY]);
            }
            $user = new UserModel();
            $user->setName($name);
            $user->setSurname($surname);
            $user->setUsername($username);
            $user->setPassword($password);
            $user->setRol_idRol("2");
            
            if($user->exists($username)){
                $this->redirect('register', ['error' => Errors::ERROR_REGISTER_NEWUSER_EXISTS]);
            }else if($user->save()){
                $this->redirect('', ['success' => Success::SUCCESS_REGISTER_NEWUSER]);
            }else{
                $this->redirect('register', ['error' => Errors::ERROR_REGISTER_NEWUSER]);
            }
        }else{
            $this->redirect('register', ['error' => Errors::ERROR_REGISTER_NEWUSER_EXISTS]);
        }
    }
}

?>