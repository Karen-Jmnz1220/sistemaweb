<?php

class Dashboard extends SessionController{

    private $user;
    private $formalitie;
    private $dept;

    function __construct(){
        parent::__construct();

        $this->user = $this->getUserSessionData();
        $this->formalitie = $this->getFormSessionData();
        $this->dept = $this->getDeptSessionData();
           
    }

    function render(){
        $idFormailitie = $this->formalitie->getIdFormalitie();
        $requirement = new FormalitiesModel();
        $stf = $this->stardedFormalitie();
        $this->view->render('dashboard/index', [
            "user" => $this->user,
            "formalitie" => $this->formalitie,
             "stf" => $stf,
            "requirement" => $requirement->getRequirement($idFormailitie)
        ]);
    }
    function stardedFormalitie(){
       
        $idFormailitie = $this->formalitie->getIdFormalitie();
        $idDept = $this->formalitie->getIdDept(); 
        $idUser = $this->user->getIdUser();
        $idManager = $this->dept->getIdManager(); 
                       
        //Datos validados
        $procedure = new DashboardModel();
        $procedure->setIdFormalitie($idFormailitie);
        $procedure->setIdUser($idUser);
        $procedure->setIdDept($idDept);
        $procedure->setIdManager($idManager);
        $procedure->setIdStep("1");
        $procedure->setIdStatus("1");
        $procedure->setAnnotation("Se ha iniciado el tramite, a la espera de revision de documentos.");
        
        $res = $procedure->startedForm();
        if($res){
            error_log("Dashboard::startedForm() idFormalitie = " . $idFormailitie);
            
        }else{
            $this->redirect('welcome/getDetailsForm', ['error' => Errors::ERROR_DASHBOARD_STARTEDFORMALITIE]);
        }
    }
    function stepUpload(){
        $idUser = $this->user->getIdUser();
        $procedure = new DashboardModel();
        $procedure->setIdStep("2");
        if($procedure->updateStepUpload()){
            $this->redirect('', ['success' => Success::SUCCESS_DASHBOARD_UPDATESTEPLOAD]);
        }else{
            //error
        }
    }
    
}

?>