<?php  

class UploadImg extends SessionController{
    //private $user;
    private $formalitie;

	function __construct(){
        parent::__construct();

        /* $this->user = $this->getUserSessionData();
        error_log("user " . $this->user->getName());  */
        $this->formalitie = $this->getFormSessionData();
    }

	function render(){        
      $this->view->errorMessage = '';
        $departments = new DepartmentsModel();
        $this->view->render('welcome',[
           'departments' => $departments->getAll(),
       ]); 
       /* $this->view->render('dashboard/index', [
        "user" => $this->user
        ]);  */
        /* $this->view->render('login/login', [
        "user" => $this->user
        ]);   */
        /* $found = $this->getLegalFound();
        $formalities = $this->getDetailsForm();
        $form = $this->getRequirement();
        $this->view->render('formalities/formalitie',[
            "formalities" => $formalities,
            "form" => $form,
            "found" => $found
        ]); */
        //$this->view->render('formalities/formaliti');
    }
    
    function uploadImg(){
        error_log("Upload::uploadImg()");
        if(!isset($_FILES['image'])){
            $this->redirect('dashboard', ['error' => Errors::ERROR_UPLOADIMG_SAVEIMG]);
            return;
        }
        $image = $_FILES['image'];

        $target_dir = "resource/img/documentation/";
        $extarr = explode('.',$image["name"]);
        $filename = $extarr[sizeof($extarr)-2];
        $ext = $extarr[sizeof($extarr)-1];
        $hash = md5(Date('Ymdgi') . $filename) . '.' . $ext;
        $target_file = $target_dir . $hash;
        $uploadOk = 1;
        $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
        
        $check = getimagesize($image["tmp_name"]);
        if($check !== false) {
            //echo "File is an image - " . $check["mime"] . ".";
            $uploadOk = 1;
        } else {
            //echo "File is not an image.";
            $uploadOk = 0;
        }

        if ($uploadOk == 0) {
            //echo "Sorry, your file was not uploaded.";
            $this->redirect('dashboard', ['error' => Errors::ERROR_UPLOADIMG_SAVEIMG_FORMAT]);
        // if everything is ok, try to upload file
        } else {
            if (move_uploaded_file($image["tmp_name"], $target_file)) {
                $uploadImgModel = new UploadImgModel();
                $uploadImgModel->saveImg($hash);
                $this->redirect('dashboard', ['success' => Success::SUCCESS_UPLOADIMG_SAVEIMAGE]);
            } else {
                $this->redirect('dashboard', ['error' => Errors::ERROR_UPLOADIMG_SAVEIMG]);
            }
        }
        
    }
}

?>