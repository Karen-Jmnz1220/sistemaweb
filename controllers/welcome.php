<?php  

class Welcome extends SessionController{
    //private $user;
    private $formalitie;

	function __construct(){
        parent::__construct();

        /* $this->user = $this->getUserSessionData();
        error_log("user " . $this->user->getName());  */
        $this->formalitie = $this->getFormSessionData();
    }

	function render(){        
        $this->view->errorMessage = '';
        $departments = new DepartmentsModel();
        $this->view->render('welcome',[
           'departments' => $departments->getAll(),
       ]);
       /* $this->view->render('dashboard/index', [
        "user" => $this->user
        ]);  */
        /* $this->view->render('login/login', [
        "user" => $this->user
        ]); */
        /* $found = $this->getLegalFound();
        $formalities = $this->getDetailsForm();
        $form = $this->getRequirement();
        $this->view->render('formalities/formalitie',[
            "formalities" => $formalities,
            "form" => $form,
            "found" => $found
        ]); */
        //$this->view->render('formalities/formaliti');
    }
 
    function getFormalitie(){
        $idDept=$_POST['idDept'];  
        error_log("Welcome::getFormalitie()");
        error_log("idDept:" .$idDept);
        $formalitiesModel = new FormalitiesModel();
        $formalities = $formalitiesModel->getFormalitie($idDept);
        $cadena = ' <option value="0" selected>Selecciona un trámite</option>';
            foreach($formalities as $form)
            {
                $cadena.= '<option value='.$form->getIdFormalitie().'>'.$form->getFormalitie_name().'</option>';
            }
            echo $cadena;      
            error_log("Welcome::getFormalitie(): " .$cadena);
    }
    function authenticate(){
        if($this->existPOST(['formalities', 'departments']) ){
            $idFormalitie = $this->getPost('formalities');
            $idDept = $this->getPost('departments');
            
            //Valida los datos
            if($idFormalitie == '' || empty($idFormalitie) || $idDept == '' || empty($idDept)){
                error_log('Welcome::authenticate() empty'); //Campos vacios
                $this->redirect('', ['error' => Errors::ERROR_WELCOME_AUTHENTICATE_EMPTY]);
                return;
            }
            // Si el login es exitoso regresa solo el ID del usuario
            $formalitie = $this->model->getForm($idFormalitie);
            $dept = $this->model->getDept($idDept);
            
            if($formalitie != NULL && $dept != NULL){
                // inicializa el proceso de las sesiones
                error_log('Welcome::authenticate() passed');    
                $this->initializeForm($formalitie);
                $this->initializeDept($dept);
                $this->redirect('welcome/getDetailsForm/', []);
            }else{    
                //Error: User y/o password incorrectos.
                error_log('Welcome::authenticate() username and/or password wrong');
                $this->redirect('', ['error' => Errors::ERROR_WELCOME_AUTHENTICATE_DATA]);
                return;
            }
        }else{
            // error, cargar vista con errores
            error_log('Welcome::authenticate() error with params');
            $this->redirect('', ['error' => Errors::ERROR_LOGIN_AUTHENTICATE]);
        }
    }
    
    function getDetailsForm(){
        error_log("Formalities::getDetailsForm()");
        
        $idFormalitie= $this->formalitie->getIdFormalitie();
        
        $formModel = new FormalitiesModel();
        $formalities = $formModel->get($idFormalitie);
        $this->view->formalities = $formalities;

        $formModel = new FormalitiesModel();
        $form = $formModel->getRequirement($idFormalitie);
        $this->view->form = $form;
      
        $legalFoundModel = new LegalFoundationsModel();
        $found = $legalFoundModel->getFound($idFormalitie);
        $this->view->found = $found;

        $this->view->render('formalities/detailsForm',[
            'formalities' => $this->formalities,
            'form'=>$this->form,
            'found'=>$this->found
        ]);
    }  
}

?>