<?php

class WelcomeModel extends Model{

    public function __construct(){
        parent::__construct();
    }

    public function getForm($idFormalitie){
        // insertar datos en la BD
        
        error_log("getForm: inicio");
        
        $query = $this->db->connect()->prepare("SELECT * FROM formalitie WHERE idFormalitie = :idFormalitie");
        try{
            $query->execute(['idFormalitie' => $idFormalitie]);
            
           
            while($row = $query->fetch()){
                $formalitie = new FormalitiesModel();
                $formalitie->from($row);
            }

            return $formalitie;
        }catch(PDOException $e){
            return null;
        }
    }
    public function getDept($idDept){
        // insertar datos en la BD
        
        error_log("getDept: inicio");
        
        $query = $this->db->connect()->prepare("SELECT * FROM departments WHERE idDept = :idDept");
        try{
            $query->execute(['idDept' => $idDept]);
            
            while($row = $query->fetch()){
                $dept = new DepartmentsModel();
                $dept->from($row);
            }

            return $dept;
        }catch(PDOException $e){
            return null;
        }
    }
}

?>