<?php

class LegalFoundationsModel extends Model implements IModel{
    
    private $idLegalFound;
    private $description;
    private $idFormalitie;

    public function __construct(){
        parent::__construct();
    }
    public function getFound($idFormalitie){
        $items = [];
        try{
            $query= $this->prepare('SELECT description FROM legal_foundations WHERE idFormalitie=:idFormalitie');
            $query->execute([ 'idFormalitie' => $idFormalitie]);

            while($p = $query->fetch(PDO::FETCH_ASSOC)){
                $item = new FormalitiesModel();
                $item->from($p); 
                
                array_push($items, $item);
            }
            return $items;
                
        }catch(PDOException $e){
            
            return false;
        }
    }
    public function save(){
        try{
            $query = $this->prepare('INSERT INTO legal_foundations (description, idFormalitie) VALUES(:description, :idFormalitie)');
            $query->execute([
                'description' => $this->description,
                'idFormalitie' => $this->idFormalitie,                
            ]);
            if($query->rowCount()) return true;

            return false;
        }catch(PDOException $e){
            return false;
        }
    }
    public function getAll(){
        $items = [];

        try{
            $query = $this->query('SELECT * FROM legal_foundations');

            while($p = $query->fetch(PDO::FETCH_ASSOC)){
                $item = new LegalFoundationsModel();
                $item->from($p); 
                
                array_push($items, $item);
            }

            return $items;

        }catch(PDOException $e){
            echo $e;
        }
    }
    
    public function get($idLegalFound){
        $items = [];
        try{
            $query= $this->prepare('SELECT * FROM legal_foundations WHERE idLegalFound=:idLegalFound');
            $query->execute([ 'idLegalFound' => $idLegalFound]);

            while($p = $query->fetch(PDO::FETCH_ASSOC)){
                $item = new LegalFoundationsModel();
                $item->from($p); 
                
                array_push($items, $item);
            }
            return $items;
                
        }catch(PDOException $e){
           echo $e;
            return false;
        }
    }
    
    public function delete($idLegalFound){
        try{
            $query = $this->db->connect()->prepare('DELETE FROM legal_foundations WHERE idLegalFound = :idLegalFound');
            $query->execute([ 'idLegalFound' => $idLegalFound]);
            return true;
        }catch(PDOException $e){
            echo $e;
            return false;
        }
    }
    public function updateAll($idLegalFound){
        try{
            $query = $this->db->connect()->prepare('UPDATE legal_foundations SET description = :description, idFormalitie = :idFormalitie WHERE idLegalFound = :idLegalFound');
            $query->execute([
                'idLegalFound' => $idLegalFound,
                'description' => $this->description,
                'idFormalitie' => $this->idFormalitie,
            ]);
            return true;
        }catch(PDOException $e){
            echo $e;
            return false;
        }
    }
    public function update(){
        try{
            $query = $this->db->connect()->prepare('UPDATE legal_foundations SET description = :description, idFormalitie = :idFormalitie WHERE idLegalFound = :idLegalFound');
            $query->execute([
                'description' => $this->description,
                'idFormalitie' => $this->idFormalitie,
            ]);
            return true;
        }catch(PDOException $e){
            echo $e;
            return false;
        }
    }
    public function exists($description){
        try{
            $query = $this->prepare('SELECT description FROM legal_foundations WHERE description = :description');
            $query->execute( ['description' => $description]);
            
            if($query->rowCount() > 0){
                error_log('LegalFoundModel::exists() => true');
                return true;
            }else{
                error_log('LegalFoundModel::exists() => false');
                return false;
            }
        }catch(PDOException $e){
            error_log($e);
            return false;
        }
    }

    public function from($array){
        $this->idLegalFound = $array['idLegalFound'];
        $this->description = $array['description'];
        $this->idFormalitie = $array['idFormalitie'];
    
    }

    public function toArray(){
        $array = [];
        
        $array['idLegalFound'] = $this->idLegalFound;
        $array['description'] = $this->description;
        $array['idFormalitie'] = $this->idFormalitie;
       
        return $array;
    }

    public function setIdLegalFound($idLegalFound){$this->idLegalFound = $idLegalFound;}
    public function setDescription($description){$this->description = $description;}
    public function setIdFormalitie($idFormalitie){$this->idFormalitie = $idFormalitie;}
    

    public function getIdLegalFound(){return $this->idLegalFound;}
    public function getDescription(){ return $this->description;}
    public function getIdFormalitie(){return $this->idFormalitie;}
    
}

?>