<?php
class ManagerModel extends Model implements IModel{

    private $idManager;
    private $manager_name;
    private $manager_surname;

    public function __construct(){
        parent::__construct();
    }

    public function save(){
        try{
            $query = $this->prepare('INSERT INTO manager (manager_name, manager_surname) VALUES(:manager_name, :manager_surname)');
            $query->execute([
                'manager_name' => $this->manager_name, 
                'manager_surname' => $this->manager_surname,
            ]);
            if($query->rowCount()) return true;

            return false;
        }catch(PDOException $e){
            return false;
        }
    }
    public function getAll(){
        $items = [];

        try{
            $query = $this->query('SELECT * FROM manager');

            while($p = $query->fetch(PDO::FETCH_ASSOC)){
                $item = new ManagerModel();
                $item->from($p); 
                
                array_push($items, $item);
            }

            return $items;

        }catch(PDOException $e){
            echo $e;
        }
    }
    
    public function get($idManager){
        try{
            $query = $this->prepare('SELECT * FROM manager WHERE idManager = :idManager');
            $query->execute([ 'idManager' => $idManager]);
            $manager = $query->fetch(PDO::FETCH_ASSOC);

            $this->from($manager);

            return $this;
        }catch(PDOException $e){
            return false;
        }
    }
    public function delete($idManager){
        try{
            $query = $this->db->connect()->prepare('DELETE FROM manager WHERE idManager = :idManager');
            $query->execute([ 'idManager' => $idManager]);
            return true;
        }catch(PDOException $e){
            echo $e;
            return false;
        }
    }
    public function updateAll($idManager){
        try{
            $query = $this->db->connect()->prepare('UPDATE manager SET manager_name = :manager_name, manager_surname = :manager_surname WHERE idManager = :idManager');
            $query->execute([
                'idManager' => $idManager, 
                'manager_name' => $this->manager_name,
                'manager_surname' => $this->manager_surname,
            ]);
            return true;
        }catch(PDOException $e){
            echo $e;
            return false;
        }
    }
    public function update(){
        try{
            $query = $this->db->connect()->prepare('UPDATE manager SET manager_name = :manager_name, manager_surname = :manager_surname WHERE idManager = :idManager');
            $query->execute([
                'idManager' => $this->idManager, 
                'manager_name' => $this->manager_name,
                'manager_surname' => $this->manager_surname,
            ]);
            return true;
        }catch(PDOException $e){
            echo $e;
            return false;
        }
    }

    public function exists($manager_name){
        try{
            $query = $this->prepare('SELECT manager_name FROM manager WHERE manager_name = :manager_name');
            $query->execute( ['manager_name' => $manager_name]);
            
            if($query->rowCount() > 0){
                error_log('ManagerModel::exists() => true');
                return true;
            }else{
                error_log('ManagerModel::exists() => false');
                return false;
            }
        }catch(PDOException $e){
            error_log($e);
            return false;
        }
    }

    public function from($array){
        $this->idManager = $array['idManager'];
        $this->manager_name = $array['manager_name'];
        $this->manager_surname = $array['manager_surname'];
    }

    public function toArray(){
        $array = [];
        $array['idManager'] = $this->idManager;
        $array['manager_name'] = $this->manager_name;
        $array['manager_surname'] = $this->manager_surname;  

        return $array;
    }

    public function setIdManager($idManager){$this->idManager = $idManager;}
    public function setManager_name($manager_name){$this->manager_name = $manager_name;}
    public function setManager_surname($manager_surname){$this->manager_surname = $manager_surname;}

    public function getIdManager(){return $this->idManager;}
    public function getManager_name(){ return $this->manager_name;}
    public function getManager_surname(){ return $this->manager_surname;}

}

?>