<?php
class ProceduresModel extends Model implements IModel{

    private $idProcedure;
    private $idFormalitie;
    private $idUser;
    private $idDept;
    private $idManager;
    private $idStep;
    private $idStatus;
    private $Annotation;
    private $formalitie_name;
    private $name;
    private $surname;
    private $name_dept;
    private $step_name;
    private $status;
    


    public function __construct(){
        parent::__construct();
    }

    public function save(){
        try{
            $query = $this->prepare('INSERT INTO procedures (idFormalitie, idUser, idDept, idManager, idStep, idStatus, Annotation) VALUES(:idFormalitie, :idUser, :idDept, :idManager, :idStep, :idStatus, :Annotation)');
            $query->execute([
                'idFormalitie' => $this->idFormalitie, 
                'idUser' => $this->idUser,
                'idDept' => $this->idDept,
                'idManager' => $this->idManager,
                'idStep' => $this->idStep,
                'idStatus' => $this->idStatus,
                'Annotation' => $this->Annotation,
            ]);
            if($query->rowCount()) return true;

            return false;
        }catch(PDOException $e){
            return false;
        }
    }
    public function getAll(){
        $items = [];

        try{
            $query = $this->query('SELECT * FROM procedures');

            while($p = $query->fetch(PDO::FETCH_ASSOC)){
                $item = new ProceduresModel();
                $item->from($p); 
                
                array_push($items, $item);
            }

            return $items;

        }catch(PDOException $e){
            echo $e;
        }
    }
    
    public function get($idManager){
        $items = [];
        try{
            $query = $this->prepare('SELECT procedures.idProcedure,formalitie.formalitie_name, users.name, users.surname,departments.name_dept, steps.step_name, status.status, procedures.Annotation FROM formalitie INNER JOIN procedures ON formalitie.idFormalitie = procedures.idFormalitie INNER JOIN users ON procedures.idUser = users.idUser INNER JOIN departments ON procedures.idDept = departments.idDept INNER JOIN steps ON procedures.idStep = steps.idStep INNER JOIN status ON procedures.idStatus = status.idStatus WHERE procedures.idManager=:idManager;');
            $query->execute([ 'idManager' => $idManager]);
            
            while($p = $query->fetch(PDO::FETCH_ASSOC)){
                $item = new ProceduresModel();
                $item->fromPro($p); 
                
                array_push($items, $item);
            }
            return $items;
        }catch(PDOException $e){
            return false;
        }
    }
   
    public function delete($idProcedure){
        try{
            $query = $this->db->connect()->prepare('DELETE FROM departments WHERE idProcedure = :idProcedure');
            $query->execute([ 'idProcedure' => $idProcedure]);
            return true;
        }catch(PDOException $e){
            echo $e;
            return false;
        }
    }
    public function updateAll($idProcedure){
        try{
            $query = $this->db->connect()->prepare('UPDATE procedure SET idFormalitie = :idFormalitie, idUser = :idUser, idDept = :idDept, idManager = :idManager, idStep = :idStep, idStatus = :idStatus, Annotation = :Annotation WHERE idProcedure = :idProcedure');
            $query->execute([
                'idProcedure' => $idProcedure,
                'idFormalitie' => $this->idFormalitie,
                'idUser' => $this->idUser, 
                'idDept' => $this->idDept,
                'idManager' => $this->idManager,
                'idStep' => $this->idStatus,
                'idStatus' => $this->idStatus,
                'Annotation' => $this->Annotation,
            ]);
            if($query->rowCount()) return true;

            return false;
        }catch(PDOException $e){
            error_log('ProceduresModel::updateAll() --> PDOException' .$e);
            return false;
        }
    }
    public function update(){
        try{
            $query = $this->db->connect()->prepare('UPDATE procedure SET idFormalitie = :idFormalitie, idUser = :idUser, idDept = :idDept, idManager = :idManager, idStep = :idStep, idStatus = :idStatus, Annotation = :Annotation WHERE idProcedure = :idProcedure');
            $query->execute([
                'idFormalitie' => $this->idFormalitie, 
                'idUser' => $this->idUser, 
                'idDept' => $this->idDept,
                'idManager' => $this->idManager,
                'idStep' => $this->idStatus,
                'idStatus' => $this->idStatus,
                'Annotation' => $this->Annotation,
            ]);
            return true;
        }catch(PDOException $e){
            error_log('ProcedureModel::update() --> PDOException' .$e);
            return false;
        }
    }

    public function exists($name_dept){
        try{
            $query = $this->prepare('SELECT name_dept FROM departments WHERE name_dept = :name_dept');
            $query->execute( ['name_dept' => $name_dept]);
            
            if($query->rowCount() > 0){
                error_log('DepartmentsModel::exists() => true');
                return true;
            }else{
                error_log('DepartmentsModel::exists() => false');
                return false;
            }
        }catch(PDOException $e){
            error_log($e);
            return false;
        }
    }

    public function from($array){
        $this->idProcedure = $array['idProcedure'];
        $this->idFormalitie = $array['idFormalitie'];
        $this->idUser = $array['idUser'];
        $this->idDept = $array['idDept'];
        $this->idManager = $array['idManager'];
        $this->idStep = $array['idStep'];
        $this->idStatus = $array['idStatus'];
        $this->Annotation = $array['Annotation'];
    }
    public function fromPro($array){
        $this->idProcedure = $array['idProcedure'];
        $this->formalitie_name = $array['formalitie_name'];
        $this->name = $array['name'];
        $this->surname = $array['surname'];
        $this->name_dept = $array['name_dept'];
        $this->idManager = $array['idManager'];
        $this->step_name = $array['step_name'];
        $this->status = $array['status'];
        $this->Annotation = $array['Annotation'];
    }

    public function toArray(){
        $array = [];
        $array['idProcedure'] = $this->idProcedure;
        $array['idFormalitie'] = $this->idFormalitie;
        $array['idUser'] = $this->idUser;
        $array['idDept'] = $this->idDept;
        $array['idStep'] = $this->idStep;
        $array['idStatus'] = $this->idStatus;
        $array['Annotation'] = $this->Annotation;  

        return $array;
    }
    public function setIdProcedure($idProcedure){$this->idProcedure = $idProcedure;}
    public function setIdUser($idUser){$this->idUser = $idUser;}
    public function setIdDept($idDept){$this->idDept = $idDept;}
    public function setIdManager($idManager){$this->idManager = $idManager;}
    public function setIdStep($idStep){$this->idStep = $idStep;}
    public function setIdStatus($idStatus){$this->idStatus = $idStatus;}
    public function setAnnotation($Annotation){$this->Annotation = $Annotation;}
    public function setFormalitie_name($idFormalitie){$this->idFormalitie = $idFormalitie;}
    public function setName($name){$this->name = $name;}
    public function setSurname($surname){$this->surname = $surname;}
    public function setStep_name($step_name){$this->step_name = $step_name;}
    public function setName_dept($name_dept){$this->name_dept = $name_dept;}
    public function setStatus($status){$this->status = $status;}


    public function getIdProcedure(){return $this->idProcedure;}
    public function getIdUser(){ return $this->idUser;}
    public function getIdDept(){ return $this->idDept;}
    public function getIdManager(){ return $this->idManager;}
    public function getIdStep(){ return $this->idStep;}
    public function getIdStatus(){ return $this->idStatus;}
    public function getAnnotation(){ return $this->Annotation;}
    public function getFormalitie_name(){ return $this->formalitie_name;}
    public function getName(){ return $this->name;}
    public function getSurname(){return $this->surname;}
    public function getStep_name(){return $this->step_name;}
    public function getName_dept(){ return $this->name_dept;}
    public function getStatus(){return $this->status;}
}

?>