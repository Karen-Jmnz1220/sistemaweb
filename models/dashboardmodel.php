<?php

class DashboardModel extends Model{

    
    Private $idFormailitie;
    Private $idUser;
    Private $idDept; 
    private $idManager;
    Private $idStep;
    Private $idStatus; 
    Private $Annotation;

    public function __construct(){
        parent::__construct();

        $this->idFormailitie = '';
        $this->idUser = '';
        $this->idDept = '';
        $this->idManager = '';
        $this->idStep = '';
        $this->idStatus = '';
        $this->Annotation = '';
    }

    public function startedForm(){
        // insertar datos en la BD
        error_log("startedForm: inicio");
        try{
            $query = $this->prepare('INSERT INTO procedures (idFormalitie, idUser, idDept, idManager, idStep, idStatus, Annotation) 
                                    VALUES(:idFormalitie, :idUser, :idDept, :idManager, :idStep, :idStatus, :Annotation)');
            $query->execute([
                'idFormalitie'      => $this->idFormailitie,
                'idUser'      => $this->idUser,
                'idDept'  => $this->idDept,
                'idManager'  => $this->idManager, 
                'idStep'  => $this->idStep,
                'idStatus'      => $this->idStatus,
                'Annotation' => $this->Annotation
                ]);
            return true;
        }catch(PDOException $e){
            error_log('DashboardModel::startedForm() --> PDOException' .$e);
            return false;
        }
    }
    function updateStepUpload($idStep, $iduser){
        try{
            $query = $this->db->connect()->prepare('UPDATE procedures SET idStep = :val WHERE idUser = :idUser');
            $query->execute(['val' => $idStep, 'id' => $iduser]);

            if($query->rowCount() > 0){
                return true;
            }else{
                return false;
            }
        
        }catch(PDOException $e){
            return NULL;
        }
    }

    public function setIdFormalitie($idFormailitie){$this->idFormailitie = $idFormailitie;}
    public function setIdUser($idUser){$this->idUser = $idUser;}
    public function setIdDept($idDept){$this->idDept = $idDept;}
    public function setIdManager($idManager){$this->idManager = $idManager;}
    public function setIdStep($idStep){$this->idStep = $idStep;}
    public function setIdStatus($idStatus){$this->idStatus = $idStatus;}
    public function setAnnotation($Annotation){$this->Annotation = $Annotation;}

    public function getIdFormalitie(){return $this->idDept;}
    public function getIdUser(){ return $this->idUser;}
    public function getIdDept(){ return $this->idDept;}
    public function getIdManager(){ return $this->idManager;}
    public function getIdStep(){ return $this->idStep;}
    public function getIdStatus(){ return $this->idStatus;}
    public function getAnnotation(){ return $this->Annotation;}
    

}

?>