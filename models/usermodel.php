<?php

class UserModel extends Model implements IModel{

    private $idUser;
    private $name;
    private $surname;
    private $username;
    private $password;
    private $rol_idRol;
    private $photo;
    

    public function __construct(){
        parent::__construct();

        $this->name = '';
        $this->surname = '';
        $this->username = '';
        $this->password = '';
        $this->rol_idRol = '';
        $this->photo = '';
        
    }


    function updateName($name, $idUser){
        try{
            $query = $this->db->connect()->prepare('UPDATE users SET name = :val WHERE idUser = :idUser');
            $query->execute(['val' => $name, 'idUser' => $idUser]);

            if($query->rowCount() > 0){
                return true;
            }else{
                return false;
            }
        
        }catch(PDOException $e){
            error_log('UserModel::updateName() --> PDOException' .$e);
            return NULL;
        }
    }
    function updateUsername($username, $idUser){
        try{
            $query = $this->db->connect()->prepare('UPDATE users SET username = :val WHERE idUser = :idUser');
            $query->execute(['val' => $username, 'idUser' => $idUser]);

            if($query->rowCount() > 0){
                return true;
            }else{
                return false;
            }
        
        }catch(PDOException $e){
            error_log('UserModel::updateUsername() --> PDOException' .$e);
            return NULL;
        }
    }

    function updatePassword($new, $idUser){
        try{
            $hash = password_hash($new, PASSWORD_DEFAULT, ['cost' => 10]);
            $query = $this->db->connect()->prepare('UPDATE users SET password = :val WHERE idUser = :idUser');
            $query->execute(['val' => $hash, 'idUser' => $idUser]);

            if($query->rowCount() > 0){
                return true;
            }else{
                return false;
            }
        
        }catch(PDOException $e){
            return NULL;
        }
    }

    function comparePasswords($current, $userid){
        try{
            $query = $this->db->connect()->prepare('SELECT idUser, password FROM users WHERE idUser = :idUser');
            $query->execute(['idUser' => $userid]);
            
            if($row = $query->fetch(PDO::FETCH_ASSOC)) return password_verify($current, $row['password']);

            return NULL;
        }catch(PDOException $e){
            return NULL;
        }
    }

    public function save(){
        try{
            $query = $this->prepare('INSERT INTO users (name, surname, username, password, rol_idRol) 
                                    VALUES(:name, :surname, :username, :password, :rol_idRol)');
            $query->execute([
                'name'      => $this->name,
                'surname'      => $this->surname,
                'username'  => $this->username, 
                'password'  => $this->password,
                'rol_idRol'      => $this->rol_idRol
                ]);
            return true;
        }catch(PDOException $e){
            error_log('UserModel::save() --> PDOException' .$e);
            return false;
        }
    } 

    public function getAll(){
        $items = [];

        try{
            $query = $this->query('SELECT * FROM users');

            while($p = $query->fetch(PDO::FETCH_ASSOC)){
                $item = new UserModel();
                $item->setIdUser($p['idUser']);
                $item->setName($p['name']);
                $item->setSurname($p['surname']);
                $item->setUsername($p['username']);
                $item->setPassword($p['password'], false);
                $item->setRol_idRol($p['rol_idRol']);
                
                

                array_push($items, $item);
            }
            return $items;


        }catch(PDOException $e){
            echo $e;
        }
    }

    /**
     *  Gets an item
     */
    public function get($idUser){
        try{
            $query = $this->prepare('SELECT * FROM users WHERE idUser = :idUser');
            $query->execute([ 'idUser' => $idUser]);
            $user = $query->fetch(PDO::FETCH_ASSOC);

            $this->idUser = $user['idUser'];
            $this->name = $user['name'];
            $this->surname = $user['surname'];
            $this->username = $user['username'];
            $this->password = $user['password'];
            $this->rol_idRol = $user['rol_idRol'];
            

            return $this;
        }catch(PDOException $e){
            return false;
        }
    }

    public function delete($idUser){
        try{
            $query = $this->prepare('DELETE FROM users WHERE idUser = :idUser');
            $query->execute([ 'idUser' => $idUser]);
            return true;
        }catch(PDOException $e){
            echo $e;
            return false;
        }
    }

    public function update(){
        try{
            $query = $this->prepare('UPDATE users SET name = :name, surname = :surname, username = :username, password = :password WHERE idUser = :idUser');
            $query->execute([
                'name' => $this->name,
                'surname' => $this->surname,
                'username' => $this->username, 
                'password' => $this->password
                                
                ]);
            return true;
        }catch(PDOException $e){
            echo $e;
            return false;
        }
    }
    public function updateAll($idUser){
        try{
            $query = $this->prepare('UPDATE users SET name = :name, surname = :surname, username = :username, 
                                    password = :password WHERE idUser = :idUser');
            $query->execute([
                'idUser' => $idUser,
                'name' => $this->name,
                'surname' => $this->surname,
                'username' => $this->username, 
                'password' => $this->password
                                
                ]);
            return true;
        }catch(PDOException $e){
            
            return false;
        }
    }

    public function exists($username){
        try{
            $query = $this->prepare('SELECT username FROM users WHERE username = :username');
            $query->execute( ['username' => $username]);
            
            if($query->rowCount() > 0){
                return true;
            }else{
                return false;
            }
        }catch(PDOException $e){
            echo $e;
            return false;
        }
    }

    public function from($array){
        $this->idUser = $array['idUser'];
        $this->name = $array['name'];
        $this->surname = $array['surname'];
        $this->username = $array['username'];
        $this->password = $array['password'];
        $this->rol_idRol = $array['rol_idRol'];
        
    }

    public function toArray(){
        $array = [];
        $array['idUser'] = $this->idUser;
        $array['name'] = $this->name;
        $array['surname'] = $this->surname;
        $array['username'] = $this->username;
        $array['password'] = $this->password;
        $array['rol_idRol'] = $this->rol_idRol;
        

        return $array;
    }

    private function getHashedPassword($password){
        return password_hash($password, PASSWORD_DEFAULT, ['cost' => 10]);
    }

    public function setUsername($username){ $this->username = $username;}
    //FIXME: validar si se requiere el parametro de hash
    public function setPassword($password, $hash = true){ 
        if($hash){
            $this->password = $this->getHashedPassword($password);
        }else{
            $this->password = $password;
        }
    }
    public function setIdUser($idUser){             $this->idUser = $idUser;}
    public function setRol_idRol($rol_idRol){         $this->rol_idRol = $rol_idRol;}
    public function setName($name){         $this->name = $name;}
    public function setSurname($surname){         $this->surname = $surname;}
    public function setPhoto($photo){       $this->photo = $photo;}

    public function getIdUser(){        return $this->idUser;}
    public function getName(){      return $this->name;}
    public function getSurname(){      return $this->surname;}
    public function getUsername(){  return $this->username;}
    public function getPassword(){  return $this->password;}
    public function getRol_idRol(){      return $this->rol_idRol;}
    public function getPhoto(){     return $this->photo;}
    
}

?>