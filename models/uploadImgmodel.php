<?php

class UploadImgModel extends Model implements IModel{

    private $idImage;
    private $image;
    private $idFormalitie;
    private $idManager;
    private $idUser;

    public function __construct(){
        parent::__construct();
        
    }
    public function saveImg($name){
        try{
            $query = $this->prepare('INSERT INTO uploadimages (image) VALUES(:val)');
            $query->execute([
                'val' => $name
            ]);
            if($query->rowCount()) return true;

            return false;
        }catch(PDOException $e){
            error_log('UploadImgModel::save() --> PDOException' .$e); 
            return false;
        }
    }
    public function save(){
        try{
            $query = $this->prepare('INSERT INTO uploadimages (image, idFormalitie, idManager, idUser) VALUES(:image, :idFormalitie, :idManager, :idUser)');
            $query->execute([
                'image' => $this->image, 
                'idFormalitie' => $this->idFormalitie,
                'idManager' => $this->idManager,
                'idUser' => $this->idUser
            ]);
            if($query->rowCount()) return true;

            return false;
        }catch(PDOException $e){
            error_log('UploadImgModel::save() --> PDOException' .$e);
            return false;
        }
    }
    public function getAll(){
        $items = [];

        try{
            $query = $this->query('SELECT * FROM uploadimages');

            while($p = $query->fetch(PDO::FETCH_ASSOC)){
                $item = new UploadImgModel();
                $item->from($p); 
                
                array_push($items, $item);
            }

            return $items;

        }catch(PDOException $e){
            echo $e;
        }
    }
    
    public function get($idManager){
        try{
            $query = $this->prepare('SELECT * FROM uploadimages WHERE idManager = :idManager');
            $query->execute([ 'idManager' => $idManager]);
            
            while($p = $query->fetch(PDO::FETCH_ASSOC)){
                $item = new UploadImgModel();
                $item->from($p); 
                
                array_push($items, $item);
            }
            return $items;
        }catch(PDOException $e){
            return false;
        }
    }
    public function delete($idImage){
        try{
            $query = $this->db->connect()->prepare('DELETE FROM uploadimages WHERE idImage = :idImage');
            $query->execute([ 'idImage' => $idImage]);
            return true;
        }catch(PDOException $e){
            echo $e;
            return false;
        }
    }
    public function updateAll($idImage){
        try{
            $query = $this->db->connect()->prepare('UPDATE uploadimages SET image = :image WHERE idImage = :idImage');
            $query->execute([
                'idImage' => $idImage,
                'image' => $this->image, 
            ]);
            if($query->rowCount()) return true;

            return false;
        }catch(PDOException $e){
            error_log('UploadImgModel::update() --> PDOException' .$e);
            return false;
        }
    }
    public function update(){
        try{
            $query = $this->db->connect()->prepare('UPDATE uploadimages SET image = :image WHERE idImage = :idImage');
            $query->execute([
                'image' => $this->image
            ]);
            return true;
        }catch(PDOException $e){
            error_log('UploadImgModel::update() --> PDOException' .$e);
            return false;
        }
    }

    public function exists($name_dept){
        try{
            $query = $this->prepare('SELECT name_dept FROM departments WHERE name_dept = :name_dept');
            $query->execute( ['name_dept' => $name_dept]);
            
            if($query->rowCount() > 0){
                error_log('DepartmentsModel::exists() => true');
                return true;
            }else{
                error_log('DepartmentsModel::exists() => false');
                return false;
            }
        }catch(PDOException $e){
            error_log($e);
            return false;
        }
    }

    public function from($array){
        $this->idImage = $array['idImage'];
        $this->image = $array['image'];
        $this->idFormalitie = $array['idFormalitie'];
        $this->idManager = $array['idManager'];
        $this->idUser = $array['idUser'];
    }

    public function toArray(){
        $array = [];
        $array['idImage'] = $this->idImage;
        $array['image'] = $this->image;
        $array['idFormalitie'] = $this->idFormalitie;
        $array['idManager'] = $this->idManager;
        $array['idUser'] = $this->idUser;

        return $array;
    }
    public function setIdImage($idImage){$this->idImage = $idImage;}
    public function setImage($image){$this->image = $image;}
    public function setIdFormalitie($idFormalitie){$this->idFormalitie = $idFormalitie;}
    public function setIdMananer($idManager){$this->idManager = $idManager;}
    public function setIdUser($idUser){$this->idUser = $idUser;}
    

    public function getIdImage(){return $this->idImage;}
    public function getImage(){ return $this->image;}
    public function getIdFormalitie(){ return $this->idFormalitie;}
    public function getIdMananer(){ return $this->idManager;}
    public function getIdUser(){return $this->idUser;}
    
}

?>