<?php
class DepartmentsModel extends Model implements IModel{

    private $idDept;
    private $name_dept;
    private $idManager;
    private $address;
    private $phone;
    private $email;


    public function __construct(){
        parent::__construct();
    }

    public function save(){
        try{
            $query = $this->prepare('INSERT INTO departments (name_dept, idManager, address, phone, email) VALUES(:name_dept, :idManager, :address, :phone, :email)');
            $query->execute([
                'name_dept' => $this->name_dept, 
                'idManager' => $this->idManager,
                'address' => $this->address,
                'phone' => $this->phone,
                'email' => $this->email,
            ]);
            if($query->rowCount()) return true;

            return false;
        }catch(PDOException $e){
            error_log('DepartmentsModel::save() --> PDOException' .$e);
            return false;
        }
    }
    public function getAll(){
        $items = [];

        try{
            $query = $this->query('SELECT * FROM departments');

            while($p = $query->fetch(PDO::FETCH_ASSOC)){
                $item = new DepartmentsModel();
                $item->from($p); 
                
                array_push($items, $item);
            }

            return $items;

        }catch(PDOException $e){
            echo $e;
        }
    }
    
    public function get($idDept){
        try{
            $query = $this->prepare('SELECT * FROM departments WHERE idDept = :idDept');
            $query->execute([ 'idDept' => $idDept]);
            $department = $query->fetch(PDO::FETCH_ASSOC);

            $this->from($department);

            return $this;
        }catch(PDOException $e){
            return false;
        }
    }
    public function delete($idDept){
        try{
            $query = $this->db->connect()->prepare('DELETE FROM departments WHERE idDept = :idDept');
            $query->execute([ 'idDept' => $idDept]);
            return true;
        }catch(PDOException $e){
            echo $e;
            return false;
        }
    }
    public function updateAll($idDept){
        try{
            $query = $this->db->connect()->prepare('UPDATE departments SET name_dept = :name_dept, idManager = :idManager, address = :address, phone = :phone, email = :email WHERE idDept = :idDept');
            $query->execute([
                'idDept' => $idDept,
                'name_dept' => $this->name_dept, 
                'idManager' => $this->idManager,
                'address' => $this->address,
                'phone' => $this->phone,
                'email' => $this->email,
            ]);
            if($query->rowCount()) return true;

            return false;
        }catch(PDOException $e){
            error_log('DepartmentsModel::update() --> PDOException' .$e);
            return false;
        }
    }
    public function update(){
        try{
            $query = $this->db->connect()->prepare('UPDATE departments SET name_dept = :name_dept, idManager = :idManager, address = :address, phone = :phone, email = :email WHERE idDept = :idDept');
            $query->execute([
                'name_dept' => $this->name_dept, 
                'idManager' => $this->idManager,
                'address' => $this->address,
                'phone' => $this->phone,
                'email' => $this->email
            ]);
            return true;
        }catch(PDOException $e){
            error_log('DepartmentsModel::update() --> PDOException' .$e);
            return false;
        }
    }

    public function exists($name_dept){
        try{
            $query = $this->prepare('SELECT name_dept FROM departments WHERE name_dept = :name_dept');
            $query->execute( ['name_dept' => $name_dept]);
            
            if($query->rowCount() > 0){
                error_log('DepartmentsModel::exists() => true');
                return true;
            }else{
                error_log('DepartmentsModel::exists() => false');
                return false;
            }
        }catch(PDOException $e){
            error_log($e);
            return false;
        }
    }

    public function from($array){
        $this->idDept = $array['idDept'];
        $this->name_dept = $array['name_dept'];
        $this->idManager = $array['idManager'];
        $this->address = $array['address'];
        $this->phone = $array['phone'];
        $this->email = $array['email'];
    }

    public function toArray(){
        $array = [];
        $array['idDept'] = $this->idDept;
        $array['name_dept'] = $this->name_dept;
        $array['idManager'] = $this->idManager;
        $array['address'] = $this->address;
        $array['phone'] = $this->phone;
        $array['email'] = $this->email;
        

        return $array;
    }
    public function setIdDept($idDept){$this->idDept = $idDept;}
    public function setName_dept($name_dept){$this->name_dept = $name_dept;}
    public function setIdManager($idManager){$this->idManager = $idManager;}
    public function setAddress($address){$this->address = $address;}
    public function setPhone($phone){$this->phone = $phone;}
    public function setEmail($email){$this->email = $email;}

    public function getIdDept(){return $this->idDept;}
    public function getName_dept(){ return $this->name_dept;}
    public function getIdManager(){ return $this->idManager;}
    public function getAddress(){ return $this->address;}
    public function getPhone(){ return $this->phone;}
    public function getEmail(){ return $this->email;}
}

?>