<?php

class FormalitiesModel extends Model implements IModel{

    private $idFormalitie;
    private $formalitie_name;
    private $description;
    private $idDept;
    private $cost;
    private $idRequirement;
    private $requirement_name;
    private $requirement_descrip;

    public function __construct(){
        parent::__construct();
    }
    public function getFormalitie($idDept){
        $items = [];
        try{
            $query= $this->prepare('SELECT * FROM formalitie WHERE idDept=:idDept');
            $query->execute([ 'idDept' => $idDept]);

            while($p = $query->fetch(PDO::FETCH_ASSOC)){
                $item = new FormalitiesModel();
                $item->from($p); 
                
                array_push($items, $item);
            }
            return $items;
                
        }catch(PDOException $e){
            error_log('FormalitieModel::getFormalitie() --> PDOException' .$e);
            return false;
        }
    }
    public function save(){
        try{
            $query = $this->prepare('INSERT INTO formalitie (formalitie_name, description, idDept, cost) VALUES(:formalitie_name, :description, :idDept, :cost)');
            $query->execute([
                'formalitie_name' => $this->formalitie_name, 
                'description' => $this->description,
                'idDept' => $this->idDept,
                'cost' => $this->cost,
                
            ]);
            if($query->rowCount()) return true;

            return false;
        }catch(PDOException $e){
            return false;
        }
    }
    public function getAll(){
        $items = [];

        try{
            $query = $this->query('SELECT * FROM formalitie');

            while($p = $query->fetch(PDO::FETCH_ASSOC)){
                $item = new FormalitiesModel();
                $item->from($p); 
                
                array_push($items, $item);
            }

            return $items;

        }catch(PDOException $e){
            echo $e;
        }
    }
    public function getId($idFormalitie){
        try{
            $query = $this->prepare('SELECT * FROM formalitie WHERE idFormalitie = :idFormalitie');
            $query->execute([ 'idFormalitie' => $idFormalitie]);
            $formalitie = $query->fetch(PDO::FETCH_ASSOC);

            $this->idFormalitie = $formalitie['idFormalitie'];
            $this->formalitie_name = $formalitie['formalitie_name'];
            $this->description = $formalitie['description'];
            $this->idDept = $formalitie['idDept'];
            $this->cost = $formalitie['cost'];
            

            return $this;
        }catch(PDOException $e){
            return false;
        }
    }
    public function get($idFormalitie){
        $items = [];
        try{
            $query= $this->prepare('SELECT * FROM formalitie WHERE idFormalitie=:idFormalitie');
            $query->execute([ 'idFormalitie' => $idFormalitie]);

            while($p = $query->fetch(PDO::FETCH_ASSOC)){
                $item = new FormalitiesModel();
                $item->from($p); 
                
                array_push($items, $item);
            }
            return $items;
                
        }catch(PDOException $e){
            error_log('FormalitieModel::getFormalitie() --> PDOException' .$e);
            return false;
        }
    }
    public function getRequirement($idFormalitie){
        $items = [];
        try{
            $query= $this->prepare('SELECT formalitie.idFormalitie, formalitie.formalitie_name, requirement.idRequirement, requirement.requirement_name, requirement.requirement_descrip 
            FROM formalitie
            INNER JOIN request ON formalitie.idFormalitie = request.idFormalitie 
            INNER JOIN requirement ON request.idRequirement = requirement.idRequirement
            WHERE formalitie.idFormalitie=:idFormalitie');
            $query->execute([ 'idFormalitie' => $idFormalitie]);

            while($p = $query->fetch(PDO::FETCH_ASSOC)){
                $item = new FormalitiesModel();
                $item->fromReq($p); 
                
                array_push($items, $item);
            }
            return $items;
                
        }catch(PDOException $e){
            error_log('FormalitieModel::getFormalitie() --> PDOException' .$e);
            return false;
        }
    }
    public function getRequirementId($idFormalitie){
        try{
            $query = $this->prepare('SELECT formalitie.idFormalitie, formalitie.formalitie_name, requirement.idRequirement, requirement.requirement_name, requirement.requirement_descrip 
            FROM formalitie
            INNER JOIN request ON formalitie.idFormalitie = request.idFormalitie 
            INNER JOIN requirement ON request.idRequirement = requirement.idRequirement
            WHERE formalitie.idFormalitie=:idFormalitie');

            $query->execute([ 'idFormalitie' => $idFormalitie]);
            $formalitie = $query->fetch(PDO::FETCH_ASSOC);
            
            $this->idFormalitie = $formalitie['idFormalitie'];
            $this->formalitie_name = $formalitie['formalitie_name'];
            $this->idRequirement = $formalitie['idRequirement'];
            $this->requirement_name = $formalitie['requirement_name'];
            $this->requirement_descrip = $formalitie['requirement_descrip'];          

            return $this;
        }catch(PDOException $e){
            return false;
        }
    }
    
    public function delete($idFormalitie){
        try{
            $query = $this->db->connect()->prepare('DELETE FROM formalitie WHERE idFormalitie = :idFormalitie');
            $query->execute([ 'idFormalitie' => $idFormalitie]);
            return true;
        }catch(PDOException $e){
            echo $e;
            return false;
        }
    }
    public function updateAll($idFormalitie){
        try{
            $query = $this->db->connect()->prepare('UPDATE formalitie SET formalitie_name = :formalitie_name, description = :description, idDept = :idDept, cost = :cost WHERE idFormalitie = :idFormalitie');
            $query->execute([
                'idFormalitie' => $idFormalitie,
                'formalitie_name' => $this->formalitie_name, 
                'description' => $this->description,
                'idDept' => $this->idDept,
                'cost' => $this->cost,
            ]);
            return true;
        }catch(PDOException $e){
            echo $e;
            return false;
        }
    }
    public function update(){
        try{
            $query = $this->db->connect()->prepare('UPDATE formalitie SET name = :name_department, description = :description, idDept = :idDept, cost = :cost, email = :email WHERE idFormalitie = :idFormalitie');
            $query->execute([
                'formalitie_name' => $this->formalitie_name, 
                'description' => $this->description,
                'idDept' => $this->idDept,
                'cost' => $this->cost,
                'email' => $this->email
            ]);
            return true;
        }catch(PDOException $e){
            echo $e;
            return false;
        }
    }

    public function exists($formalitie_name){
        try{
            $query = $this->prepare('SELECT formalitie_name FROM formalitie WHERE formalitie_name = :formalitie_name');
            $query->execute( ['formalitie_name' => $formalitie_name]);
            
            if($query->rowCount() > 0){
                error_log('formalitieModel::exists() => true');
                return true;
            }else{
                error_log('formalitieModel::exists() => false');
                return false;
            }
        }catch(PDOException $e){
            error_log($e);
            return false;
        }
    }

    public function from($array){
        $this->idFormalitie = $array['idFormalitie'];
        $this->formalitie_name = $array['formalitie_name'];
        $this->description = $array['description'];
        $this->idDept = $array['idDept'];
        $this->cost = $array['cost'];
        
    }
    public function fromReq($array){
        $this->idFormalitie = $array['idFormalitie'];
        $this->formalitie_name = $array['formalitie_name'];
        $this->idRequirement = $array['idRequirement'];
        $this->requirement_name = $array['requirement_name'];
        $this->requirement_descrip = $array['requirement_descrip'];
        
    }

    public function toArray(){
        $array = [];
        $array['idFormalitie'] = $this->idFormalitie;
        $array['formalitie_name'] = $this->formalitie_name;
        $array['description'] = $this->description;
        $array['idDept'] = $this->idDept;
        $array['cost'] = $this->cost;

        return $array;
    }

    public function setIdFormalitie($idFormalitie){$this->idFormalitie = $idFormalitie;}
    public function setFormalitie_name($formalitie_name){$this->formalitie_name = $formalitie_name;}
    public function setDescription($description){$this->description = $description;}
    public function setIdDept($idDept){$this->idDept = $idDept;}
    public function setCost($cost){$this->cost = $cost;}
    

    public function getIdFormalitie(){return $this->idFormalitie;}
    public function getFormalitie_name(){ return $this->formalitie_name;}
    public function getDescription(){ return $this->description;}
    public function getIdDept(){ return $this->idDept;}
    public function getCost(){ return $this->cost;}
    public function getIdRequirement(){ return $this->idRequirement;}
    public function getRequirement_name(){ return $this->requirement_name;}
    public function getRequirement_descrip(){ return $this->requirement_descrip;}
    
}

?>