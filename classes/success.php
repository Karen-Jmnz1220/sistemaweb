<?php

class Success{
    //ERROR|SUCCESS
    //Controller
    //method
    //operation
    
    const SUCCESS_USER_UPDATENAME        = "5906512ea5c1ade008a94dd88ac3b7bc";
    const SUCCESS_USER_UPDATEPASSWORD    = "251d974a5234c85fecfe546f5d77accd";
    const SUCCESS_REGISTER_NEWUSER       = "8281e04ed52ccfc13820d0f6acb0985a";
    const SUCCESS_ADMIN_NEWDEPTS         = "4546e09ed52ccfc13820d0f6acb0789a";
    const SUCCESS_RESOURCE_DELETE        = "7896e97rf52vvfc13820j0f6ack0567a";
    const SUCCESS_ADMIN_UPDATEUSER       = "5792fc4c2ecad7183008c0c0a3ca66ee";
    const SUCCESS_ADMIN_NEWDFORMALITIE   = "7d1430390ae4a8016fd13eabb37ef27f";
    const SUCCESS_ADMIN_NEWDMANAGER      = "50ff33361f73df5086fbc47871370566";
    const SUCCESS_ADMIN_NEWUSER          = "4caf0c294e2eab71fe2fa7f31dbabe95";
    const SUCCESS_ADMIN_UPDATEDEPT       = "4a3ae4541a12879e1ad750edb4857e69";
    const SUCCESS_USER_UPDATEPHOTO       = "edabc9e4581fee3f0056fff4685ee9a8";
    const SUCCESS_ADMIN_UPDATEFORMALITIE = "fb3ee9486ad06f04112cd210ae3e7f57";
    const SUCCESS_ADMIN_UPDATEMANAGER    = "a44f7ddb1a1385851017d37584dfe307";
    const SUCCESS_USER_UPDATEUSERNAME    = "77284f4f74998718a6eca405b30dc831";
    const SUCCES_WELCOME_AUTHENTICATE    = "94c18078a1974beb52c72454feff3be6";
    const SUCCESS_DASHBOARD_STATEDFORMALITIE = "b7c68c5882d42f6bfbf63e6428658940";
    const SUCCESS_UPLOADIMG_SAVEIMAGE    = "5dbd6b9d3e474362ae27270d8e8d0c7d";

    private $successList = [];

    public function __construct()
    {
        $this->successList = [
            
            Success::SUCCESS_USER_UPDATENAME      => "Nombre actualizado correctamente",
            Success::SUCCESS_USER_UPDATEPASSWORD  => "Contraseña actualizado correctamente",
            Success::SUCCESS_REGISTER_NEWUSER     => "Usuario registrado correctamente",
            Success::SUCCESS_ADMIN_NEWDEPTS       => "Departamento registrado correctamente",
            Success::SUCCESS_RESOURCE_DELETE      => "Ha sido eliminado correctamente",
            Success::SUCCESS_ADMIN_UPDATEUSER     => "Usuario actualizado correctamente",
            Success::SUCCESS_ADMIN_NEWDFORMALITIE => "Trámite registrado correctamente",
            Success::SUCCESS_ADMIN_NEWDMANAGER    => "Encargado del dept se registro correctamente",
            Success::SUCCESS_ADMIN_NEWUSER        => "Usuario registrado correctamente",
            Success::SUCCESS_ADMIN_UPDATEDEPT     => "Departamento registrado correctamente",
            Success::SUCCESS_USER_UPDATEPHOTO     => "Imagen de usuario actualizada correctamente",
            Success::SUCCESS_ADMIN_UPDATEFORMALITIE => 'Trámite actualizado correctamente',
            Success::SUCCESS_ADMIN_UPDATEMANAGER => 'Encargado del Dept actualizado correctamente',
            Success::SUCCESS_USER_UPDATEUSERNAME => 'El username se ha actualizado correctamente',
            SUCCESS::SUCCES_WELCOME_AUTHENTICATE  => 'Guardado con exito',
            SUCCESS::SUCCESS_DASHBOARD_STATEDFORMALITIE => 'Tramite iniciado correctaente',
            Success::SUCCESS_UPLOADIMG_SAVEIMAGE    => 'Imagen subida correctamente'
        ];
    }

    function get($hash){
        return $this->successList[$hash];
    }

    function existsKey($key){
        if(array_key_exists($key, $this->successList)){
            return true;
        }else{
            return false;
        }
    }
}
?>