<?php

class Errors{
    //ERROR|SUCCESS
    //Controller
    //method
    //operation
    
    
    
    const ERROR_USER_UPDATENAME_EMPTY           = "0f0735f8603324a7bca482debdf088fa";
    const ERROR_USER_UPDATENAME                 = "98217b0c263b136bf14925994ca7a0aa";
    const ERROR_USER_UPDATEPASSWORD             = "365009a3644ef5d3cf7a229a09b4d690";
    const ERROR_USER_UPDATEPASSWORD_EMPTY        = "0f0735f8603324a7bca482debdf088fa";
    const ERROR_USER_UPDATEPASSWORD_ISNOTTHESAME = "27731b37e286a3c6429a1b8e44ef3ff6";
    const ERROR_LOGIN_AUTHENTICATE               = "11c37cfab311fbe28652f4947a9523c4";
    const ERROR_LOGIN_AUTHENTICATE_EMPTY         = "2194ac064912be67fc164539dc435a42";
    const ERROR_LOGIN_AUTHENTICATE_DATA          = "bcbe63ed8464684af6945ad8a89f76f8";
    const ERROR_REGISTER_NEWUSER                 = "1fdce6bbf47d6b26a9cd809ea1910222";
    const ERROR_REGISTER_NEWUSER_EMPTY           = "a5bcd7089d83f45e17e989fbc86003ed";
    const ERROR_REGISTER_NEWUSER_EXISTS          = "a74accfd26e06d012266810952678cf3";
    const ERROR_DEPARTMENTS_GETDEPT_EMPTY        = "1ewv74addfd26e06d012266810123678cf3";
    const ERROR_ADMIN_NEWDEPTS_EXISTS            = "2ewv45addfd76e06d013366810123678cg5";
    const ERROR_ADMIN_NEWDEPT_EXISTS             ="123ewv45addfd76e06d013366810123678cg5";
    const ERROR_ADMIN_NEWDEPTS                   = "78ewv45addfd76e06d013366810123678cg5";
    const ERROR_ADMIN_NEWDEPTS_EMPTY             = "4efg89addfd26e06d012266810123678cf3";
    const ERROR_ADMIN_DELETE                     = "dele90ed8464684af6945te8a89f76f8";
    const ERROR_ADMIN_UPDATE_EMPTY               = "6905b694725f674cda489a95bd422dbe";
    const ERROR_ADMIN_UPDATEUSER_EXISTS          = "5c62794894aa79eb63d6c2241bcef548";
    const ERROR_ADMIN_UPDATEUSER                 = "521a88b3fd90100f1d352020e1464693";
    const ERROR_ADMIN_NEWUSER_EXISTS             = "047dee7ca749a515fd0e31493a7dcb5a";
    const ERROR_ADMIN_NEWFORMALITIE_EXISTS       = "d7580794df2089ac0085e4ab33361073";
    const ERROR_ADMIN_NEWFORMALITIE              = "f22111aebbb01a0a36e2ef8989ba1ada";
    const ERROR_ADMIN_NEWMANAGER_EMPTY           = "f84b65c9e3c4dbc53c690da860e1e983";
    const ERROR_ADMIN_NEWMANAGER_EXISTS          = "a6549062e7351b9ce0655fd1a2769356";
    const ERROR_ADMIN_NEWMANAGER                 = "bb02e6417d83e232c1fe32ca5a29a62b";
    const ERROR_ADMIN_NEWDUSER_EMPTY             = "6337a69e622080e659513caa6e955803";
    const ERROR_ADMIN_NEWUSERS                   = "4bb3c61b6c4c5c773b615d2062f9a2d1";
    const ERROR_ADMIN_UPDATEDEPT                 = "9b3633858d7fb25b968259b66a1fd4d1";
    const ERROR_ADMIN_UPDATEDEPT_EXISTS          = "c7591f86972d87795cb9268a438c671d";
    const ERROR_USER_UPDATEPHOTO                 = "dfb4dc6544b0dae81ea132de667b2a5d";
    const ERROR_USER_UPDATEPHOTO_FORMAT          = "53f3554f0533aa9f20fbf46bd5328430";
    const ERROR_ADMIN_UPDATEFORMALITIE_EXISTS    = "75909dea6b18fea924a63f34a4b8e208";
    const ERROR_ADMIN_UPDATEFORMALITIE           = "54c3aadb7fe18adea5aa19f5f10e2420";
    const ERROR_ADMIN_UPDATEMANAGER_EXISTS       = "9b5debebfb0ee606ea55e59697a7a5c9"; 
    const ERROR_ADMIN_UPDATEMANAGER              = "56793b0c65a17e3d5e2ab80a11732654";
    const ERROR_USER_UPDATEUSERNAME              = "d12dc85c9340879d6c67e87fa03a7035";
    const ERROR_WELCOME_AUTHENTICATE_EMPTY       = "c8d39bf8575e301ac977c863fe540214";
    const ERROR_WELCOME_AUTHENTICATE_DATA        = "abfd44a87fb8d80bc2840a263fd5ae30";
    const ERROR_DASHBOARD_STARTEDFORMALITIE      = "c89d23c83d956773a65b7c6cc64f7d05";
    const ERROR_UPLOADIMG_SAVEIMG                = "fb4dcab996d5a02d47b4fc51a503c900";
    const ERROR_UPLOADIMG_SAVEIMG_FORMAT         = "5b4aa5a683d48c987beee38c1d3d3de3";
    
    private $errorsList = [];

    public function __construct()
    {
        $this->errorsList = [
            
            Errors::ERROR_USER_UPDATENAME_EMPTY     => 'El nombre no puede estar vacio o ser negativo',
            Errors::ERROR_USER_UPDATENAME           => 'No se puede actualizar el nombre',
            Errors::ERROR_USER_UPDATEPASSWORD       => 'No se puede actualizar la contraseña',
            Errors::ERROR_USER_UPDATEPASSWORD_EMPTY => 'El nombre no puede estar vacio o ser negativo',
            Errors::ERROR_USER_UPDATEPASSWORD_ISNOTTHESAME => 'Los passwords no son los mismos',
            Errors::ERROR_LOGIN_AUTHENTICATE        => 'Hubo un problema al autenticarse',
            Errors::ERROR_LOGIN_AUTHENTICATE_EMPTY  => 'Los parámetros para autenticar no pueden estar vacíos',
            Errors::ERROR_LOGIN_AUTHENTICATE_DATA   => 'Nombre de usuario y/o password incorrectos',
            Errors::ERROR_REGISTER_NEWUSER            => 'Hubo un error al intentar registrarte. Intenta de nuevo',
            Errors::ERROR_REGISTER_NEWUSER_EMPTY      => 'Los campos no pueden estar vacíos',
            Errors::ERROR_REGISTER_NEWUSER_EXISTS     => 'El nombre de usuario ya existe, selecciona otro',
            Errors::ERROR_DEPARTMENTS_GETDEPT_EMPTY => 'Seleccione un paramentro para validar',
            Errors::ERROR_ADMIN_NEWDEPTS_EXISTS => 'El nombre del departamento ya existe, selecciona otro',
            Errors::ERROR_ADMIN_NEWDEPTS  => 'No se ha podido registrar el departamento correctamente',
            Errors::ERROR_ADMIN_NEWDEPTS_EMPTY => 'Los campos no puede estar vacios',
            Errors::ERROR_ADMIN_DELETE => 'No se ha podido eliminar correctamente',
            Errors::ERROR_ADMIN_UPDATE_EMPTY => 'Los campos no puden estar vacios',
            Errors::ERROR_ADMIN_UPDATEUSER_EXISTS => 'El usuario ya existe, elija uno distinto',
            Errors::ERROR_ADMIN_UPDATEUSER    => 'Hubo un error al actualizar al usuario. Intentelo mas tarde',
            Errors::ERROR_ADMIN_NEWUSER_EXISTS => 'Hubo un error al registrar al usuario. Intentelo mas tarde',
            Errors::ERROR_ADMIN_NEWFORMALITIE_EXISTS => 'El nombre del tramite ya existe, elige otro',
            Errors::ERROR_ADMIN_NEWFORMALITIE        => 'Hubo un error al registrar el trámite',
            Errors::ERROR_ADMIN_NEWMANAGER_EMPTY     => 'Seleccione un paramentro para validar',
            Errors::ERROR_ADMIN_NEWMANAGER_EXISTS    => 'El nombre del Encargado ya existe, elige otro',
            Errors::ERROR_ADMIN_NEWMANAGER           => 'Ha habido un error al registrar al Encargado',
            Errors::ERROR_ADMIN_NEWDUSER_EMPTY       => 'Los campos no pueden estar vacios',
            Errors::ERROR_ADMIN_NEWUSERS             => 'No se ha podido registrar al usuario',
            Errors::ERROR_ADMIN_UPDATEDEPT => 'No existen paramentros',
            Errors::ERROR_ADMIN_UPDATEDEPT_EXISTS => 'El nombre del departamento ya existe, selecciona otro',
            Errors::ERROR_USER_UPDATEPHOTO          => 'Hubo un error al actualizar la foto',
            Errors::ERROR_USER_UPDATEPHOTO_FORMAT   => 'El archivo no es una imagen',
            Errors::ERROR_ADMIN_UPDATEFORMALITIE_EXISTS => 'El nombre del tramite ya existe, selecciona otro',
            Errors::ERROR_ADMIN_UPDATEFORMALITIE    => 'Hubo un error en actualizar el tramite',
            Errors::ERROR_ADMIN_UPDATEMANAGER_EXISTS => 'El nombre del encargado ya existe, selecciona otro',
            Errors::ERROR_ADMIN_UPDATEMANAGER        => 'Hubo un error al actualizar el Manager',
            Errors::ERROR_USER_UPDATEUSERNAME        =>'Hubo un error al actualzar el Username',
            Errors::ERROR_WELCOME_AUTHENTICATE_EMPTY =>'Seleccione un tramite',
            Errors::ERROR_WELCOME_AUTHENTICATE_DATA =>  'Tramite incorrecto',
            Errors::ERROR_DASHBOARD_STARTEDFORMALITIE => 'No se ha iniciado el tramite correctamente',
            Errors::ERROR_UPLOADIMG_SAVEIMG          => 'Seleccione una imagen',
            Errors::ERROR_UPLOADIMG_SAVEIMG_FORMAT   => 'Formato de imagen incorrecto'
        ];
    }

    function get($hash){
        return $this->errorsList[$hash];
    }

    function existsKey($key){
        if(array_key_exists($key, $this->errorsList)){
            return true;
        }else{
            return false;
        }
    }
}
?>