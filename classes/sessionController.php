<?php
/**
 * Controlador que también maneja las sesiones
 */
class SessionController extends Controller{
    
    private $userSession;
    private $username;
    private $userid;
    
    private $formSession;
    private $formalitie_name;
    private $idFormalitie;

    private $deptSession;
    private $name_dept;
    private $idDept;


    private $session;
    private $sessionForm;
    private $sessionDept;
    private $sites;

    private $user;
    private $formalitie;
    private $dept;
 
    function __construct(){
        parent::__construct();

        $this->init();
        $this->initForm();
        $this->initDept();   
    }

    public function getUserSession(){
        return $this->userSession;
    }

    public function getUsername(){
        return $this->username;
    }

    public function getUserId(){
        return $this->userid;
    }
    public function getFormSession(){
        return $this->formSession;
    }

    public function getFormalitie_name(){
        return $this->formalitie_name;
    }

    public function getIdFormalitie(){
        return $this->idFormalitie;
    }

    public function getDeptSession(){
        return $this->deptSession;
    }

    public function getName_dept(){
        return $this->name_dept;
    }

    public function getIdDept(){
        return $this->idDept;
    }

    /**
     * Inicializa el parser para leer el .json
     */
    private function init(){
        //se crea nueva sesión
        $this->session = new Session();
        //se carga el archivo json con la configuración de acceso
        $json = $this->getJSONFileConfig();
        // se asignan los sitios
        $this->sites = $json['sites'];
        // se asignan los sitios por default, los que cualquier rol tiene acceso
        $this->defaultSites = $json['default-sites'];
        // inicia el flujo de validación para determinar
        // el tipo de rol y permismos
        $this->validateSession();
    }

    private function initForm(){
        //se crea nueva sesión
        $this->sessionForm = new Session();
        $this->validateSessionForm();
    }
    private function initDept(){
        //se crea nueva sesión
        $this->sessionDept = new Session();
        $this->validateSessionDept();
    }
    /**
     * Abre el archivo JSON y regresa el resultado decodificado
     */
    private function getJSONFileConfig(){
        $string = file_get_contents("config/access.json");
        $json = json_decode($string, true);

        return $json;
    }

    /**
     * Implementa el flujo de autorización
     * para entrar a las páginas
     */
    function validateSession(){
        error_log('SessionController::validateSession()');
        //Si existe la sesión
        if($this->existsSession()){
            $rol_idRol = $this->getUserSessionData()->getRol_idRol();

            error_log("sessionController::validateSession(): username:" . $this->user->getUsername() . " - rol_idRol: " . $this->user->getRol_idRol());
            if($this->isPublic()){
                $this->redirectDefaultSiteByrol_idRol($rol_idRol);
                error_log( "SessionController::validateSession() => sitio público, redirige al main de cada rol" );
            }else{
                if($this->isAuthorized($rol_idRol)){
                    error_log( "SessionController::validateSession() => autorizado, lo deja pasar" );
                    //si el usuario está en una página de acuerdo
                    // a sus permisos termina el flujo
                }else{
                    error_log( "SessionController::validateSession() => no autorizado, redirige al main de cada rol" );
                    // si el usuario no tiene permiso para estar en
                    // esa página lo redirije a la página de inicio
                    $this->redirectDefaultSiteByrol_idRol($rol_idRol);
                }
            }
        }else{
            //No existe ninguna sesión
            //se valida si el acceso es público o no
            if($this->isPublic()){
                error_log('SessionController::validateSession() public page');
                //la pagina es publica
                //no pasa nada
            }else{
                //la página no es pública
                //redirect al login
                error_log('SessionController::validateSession() redirect al login');
                header('location: '. constant('URL') . '');
            }
        }
    }
    /**
     * Valida si existe sesión, 
     * si es verdadero regresa el usuario actual
     */
    function validateSessionForm(){
        error_log('SessionController::validateSessionForm()');
        //Si existe la sesión
        if($this->existsSessionForm()){
           $this->getFormSessionData();

            error_log("sessionController::validateSessionForm(): formalitie_name:" . $this->formalitie->getFormalitie_name());
        }
    }
    function validateSessionDept(){
        error_log('SessionController::validateSessionDept()');
        //Si existe la sesión
        if($this->existsSessionDept()){
           $this->getDeptSessionData();

            error_log("sessionController::validateSessionDept(): dept_name:" . $this->dept->getName_dept());
        }
    }

    function existsSession(){
        if(!$this->session->exists()) return false;
        if($this->session->getCurrentUser() == NULL) return false;

        $userid = $this->session->getCurrentUser();

        if($userid) return true;

        return false;
    }
    function existsSessionForm(){
        if(!$this->sessionForm->existsForm()) return false;
        if($this->sessionForm->getCurrentForm() == NULL) return false;

        $idFormalitie = $this->sessionForm->getCurrentForm();

        if($idFormalitie) return true;

        return false;
    }

    function existsSessionDept(){
        if(!$this->sessionDept->existsDept()) return false;
        if($this->sessionDept->getCurrentDept() == NULL) return false;

        $idDept = $this->sessionDept->getCurrentForm();

        if($idDept) return true;

        return false;
    }

    function getUserSessionData(){
        $idUser = $this->session->getCurrentUser();
        $this->user = new UserModel();
        $this->user->get($idUser);
        error_log("sessionController::getUserSessionData(): " . $this->user->getUsername());
        return $this->user;
    }

    function getFormSessionData(){
        $idFormalitie = $this->sessionForm->getCurrentForm();
        error_log("sessionController::getFormSessionData(): idForm: " . $idFormalitie);
        $this->formalitie = new FormalitiesModel();
        $this->formalitie->getId($idFormalitie);
        $this->formalitie->getRequirementId($idFormalitie);
        error_log("sessionController::getFormSessionData(): " . $this->formalitie->getFormalitie_name());
        error_log("sessionController::getFormSessionData(): " . $this->formalitie->getRequirement_name());
        return $this->formalitie;
    }
    function getDeptSessionData(){
        $idDept = $this->sessionDept->getCurrentDept();
        error_log("sessionController::getDeptSessionData(): idDept: " . $idDept);
        $this->dept = new DepartmentsModel();
        $this->dept->get($idDept);
        error_log("sessionController::getDeptSessionData(): " . $this->dept->getName_dept());
    
        return $this->dept;
    }

    public function initialize($user){
        error_log("sessionController::initialize(): user: " . $user->getUsername());
        $this->session->setCurrentUser($user->getidUser());
        $this->authorizeAccess($user->getRol_idRol());
    }

    public function initializeForm($formalitie){
        
        error_log("sessionController::initializeForm(): formalitie: " . $formalitie->getFormalitie_name());
        $this->sessionForm->setCurrentForm($formalitie->getIdFormalitie());
    }
    public function initializeDept($dept){
        
        error_log("sessionController::initializeDept(): dept: " . $dept->getName_dept());
        $this->sessionDept->setCurrentDept($dept->getIdDept());
    }

    private function isPublic(){
        $currentURL = $this->getCurrentPage();
        error_log("sessionController::isPublic(): currentURL => " . $currentURL);
        $currentURL = preg_replace( "/\?.*/", "", $currentURL); //omitir get info
        for($i = 0; $i < sizeof($this->sites); $i++){
            if($currentURL === $this->sites[$i]['site'] && $this->sites[$i]['access'] === 'public'){
                return true;
            }
        }
        return false;
    }

    private function redirectDefaultSiteByrol_idRol($rol_idRol){
        $url = '';
        for($i = 0; $i < sizeof($this->sites); $i++){
            if($this->sites[$i]['rol_idRol'] === $rol_idRol){
                $url = '/sistemaweb/'.$this->sites[$i]['site'];
            break;
            }
        }
        header('location: '.$url);
        
    }

    private function isAuthorized($rol_idRol){
        $currentURL = $this->getCurrentPage();
        $currentURL = preg_replace( "/\?.*/", "", $currentURL); //omitir get info
        
        for($i = 0; $i < sizeof($this->sites); $i++){
            if($currentURL === $this->sites[$i]['site'] && $this->sites[$i]['rol_idRol'] === $rol_idRol){
                return true;
            }
        }
        return false;
    }

    private function getCurrentPage(){
        
        $actual_link = trim("$_SERVER[REQUEST_URI]");
        $url = explode('/', $actual_link);
        error_log("sessionController::getCurrentPage(): actualLink =>" . $actual_link . ", url => " . $url[2]);
        return $url[2];
    }

    function authorizeAccess($rol_idRol){
        error_log("sessionController::authorizeAccess(): rol_idRol: $rol_idRol");
        switch($rol_idRol){
            case '1':
                $this->redirect($this->defaultSites['1']);
            break;
            case '2':
                $this->redirect($this->defaultSites['2']);
            break;
            case '3':
                $this->redirect($this->defaultSites['3']);
            break;
            default:
        }
    }

    function logout(){
        $this->session->closeSession();
    }
    function deleteForm(){
        $this->sessionForm->closeSessionForm();
        $this->sessionDept->closeSessionDept();

    }
}


?>