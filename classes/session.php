<?php

class Session{

    private $sessionName = 'user';
    private $sessionNameForm = 'formalitie';

    public function __construct(){
        if (session_status() == PHP_SESSION_NONE) {
            session_start();
        }
    }

    public function setCurrentUser($user){
        $_SESSION[$this->sessionName] = $user;
    }
    public function setCurrentForm($formalitie){
        $_SESSION[$this->sessionNameForm] = $formalitie;
    }
    public function setCurrentDept($dept){
        $_SESSION[$this->sessionNameDept] = $dept;
    }
    public function getCurrentUser(){
        return $_SESSION[$this->sessionName];
    }
    public function getCurrentForm(){
        return $_SESSION[$this->sessionNameForm];
    }
    public function getCurrentDept(){
        return $_SESSION[$this->sessionNameDept];
    }

    public function closeSession(){
        session_unset();
        session_destroy();
    }
    public function closeSessionForm(){
        session_unset();
        session_destroy();
    }
    public function closeSessionDept(){
        session_unset();
        session_destroy();
    }

    public function exists(){
        return isset($_SESSION[$this->sessionName]);
    }
    public function existsForm(){
        return isset($_SESSION[$this->sessionNameForm]);
    }
    public function existsDept(){
        return isset($_SESSION[$this->sessionNameDept]);
    }
}

?>